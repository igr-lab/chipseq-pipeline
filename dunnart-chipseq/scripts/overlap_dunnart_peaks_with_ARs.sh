## Overlapping dunnart peaks with thylacine-wolf ARs.

module load blast
module load bedtools/2.30.0

# use blast to find the coordinates for the devil TWARs in the dunnart genome
blastn -task blastn -db /data/projects/punim0586/lecook/chipseq-pipeline/dunnart/genomes/Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr.fasta -query ${tr}.fa -out ${tr}.blastout.txt -outfmt 7

# get top hit
cat ${tr}.blastout.txt |awk '/hits found/{getline;print}' | grep -v "#" > ${tr}_top_hits.txt
awk '{print $2 "\t" $10 "\t" $9 "\t" $1}' dunnart_twars_top_hits.txt > dunnart_twars_top_hits2.bed

# bedtools intersect H3K4me3 and H3K27ac peaks with thylacine-wolf ARs
bedtools intersect -a dunnart_twars_top_hits.bed -b H3K4me3_overlap.narrowPeak > dunnart_twars_H3K4me3.bed 
bedtools intersect -a dunnart_twars_top_hits.bed -b H3K27ac_overlap.narrowPeak > dunnart_twars_H3K27ac.bed

# bedtools intersect promoter and enhancer-associated peaks with thylacine-wolf ARs
bedtools intersect -a dunnart_twars_top_hits.bed -b dunnart_enhancers.narrowPeak > dunnart_twars_enhancers.bed 
bedtools intersect -a dunnart_twars_top_hits.bed -b cluster1_promoters.narrowPeak > dunnart_twars_promoters.bed

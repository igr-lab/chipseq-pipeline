# installed HOMER with the following modules loaded
module load perl
module load gcc
module load web_proxy
module load wget

## GC content of promoters and enhancers using homer

annotatePeaks.pl dunnart_promoters.narrowPeak smiCra1 -gff Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff -CpG -cons > dunnart_promoters_homerAnnot.txt
annotatePeaks.pl dunnart_enhancers.narrowPeak smiCra1 -gff Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff -CpG -cons > dunnart_enhancers_homerAnnot.txt 

annotatePeaks.pl enhancers/dunnart_enhancer_peaks.narrowPeak smiCra1 -gff ../../../dunnart/genomes/Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff -CpG > enhancers/dunnart_enhancers_homerAnnot.txt &
annotatePeaks.pl enhancers/E10.5_enhancer_peaks.narrowPeak mm10 -CpG > enhancers/E10.5_enhancers_homerAnnot.txt &
annotatePeaks.pl enhancers/E11.5_enhancer_peaks.narrowPeak mm10 -CpG > enhancers/E11.5_enhancers_homerAnnot.txt &
annotatePeaks.pl enhancers/E12.5_enhancer_peaks.narrowPeak mm10 -CpG > enhancers/E12.5_enhancers_homerAnnot.txt &
annotatePeaks.pl enhancers/E13.5_enhancer_peaks.narrowPeak mm10 -CpG > enhancers/E13.5_enhancers_homerAnnot.txt &
annotatePeaks.pl enhancers/E14.5_enhancer_peaks.narrowPeak mm10 -CpG > enhancers/E14.5_enhancers_homerAnnot.txt &
annotatePeaks.pl enhancers/E15.5_enhancer_peaks.narrowPeak mm10 -CpG > enhancers/E15.5_enhancers_homerAnnot.txt &

annotatePeaks.pl promoters/dunnart_promoter_peaks.narrowPeak smiCra1 -gff ../../../dunnart/genomes/Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff -CpG > promoters/dunnart_promoters_homerAnnot.txt &
annotatePeaks.pl promoters/E10.5_promoter_peaks.narrowPeak mm10 -CpG > promoters/E10.5_promoters_homerAnnot.txt &
annotatePeaks.pl promoters/E11.5_promoter_peaks.narrowPeak mm10 -CpG > promoters/E11.5_promoters_homerAnnot.txt &
annotatePeaks.pl promoters/E12.5_promoter_peaks.narrowPeak mm10 -CpG > promoters/E12.5_promoters_homerAnnot.txt &
annotatePeaks.pl promoters/E13.5_promoter_peaks.narrowPeak mm10 -CpG > promoters/E13.5_promoters_homerAnnot.txt &
annotatePeaks.pl promoters/E14.5_promoter_peaks.narrowPeak mm10 -CpG > promoters/E14.5_promoters_homerAnnot.txt &
annotatePeaks.pl promoters/E15.5_promoter_peaks.narrowPeak mm10 -CpG > promoters/E15.5_promoters_homerAnnot.txt &

## GC and CpG on promoter clusters
annotatePeaks.pl cluster1_promoters.narrowPeak smiCra1 -gff Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff -CpG -cons > cluster1_promoters_homerAnnot.txt
annotatePeaks.pl cluster2_promoters.narrowPeak smiCra1 -gff Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff -CpG -cons > cluster2_promoters_homerAnnot.txt
annotatePeaks.pl cluster3_promoters.narrowPeak smiCra1 -gff Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff -CpG -cons > cluster3_promoters_homerAnnot.txt


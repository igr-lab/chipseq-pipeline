

library(ChIPseeker)
library(data.table)
library(dplyr)
library(tidyverse)
library(GenomicFeatures)

# x, q: vector of quantiles representing the number of white balls
#           drawn without replacement from an urn which contains both
#           black and white balls. 106

#        m: the number of white balls in the urn. 

#        n: the number of black balls in the urn. 14295 twars wihtout peaks

#        k: the number of balls drawn from the urn, hence must be in
#           0,1,..., m+n. 4000 genes with peaks

# These are mouse ENSEMBL IDs for genes where there is an orthologous gene in the Tas devil 


# q = genes with dunnart peak and TWAR
# m = genes with TWARs
# n = genes with no twar
# k = genes with dunnart peaks that could have had a TWAR

## Make txdb for dunnart annotation file
setwd("/data/projects/punim0586/lecook/chipseq-pipeline/cross_species/peakAnnotation/")
smiCra_txdb <- makeTxDbFromGFF("Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff")
bg=fread("go_background_orthologousENSEMBL_biomart.txt", header=FALSE, sep="\t", quote = "", na.strings=c("", "NA"))
bg = unique(unlist(bg, use.names=F, recursive = T))

## geneID conversion tables
df2 <- read.table("biomart_ensembl.txt", header=TRUE, sep="\t")
df3 <- read.table("bioDBnet_refseq2ensembl_H3K27ac.txt", header=TRUE, sep="\t")

setwd("/data/projects/punim0586/lecook/functional_validation_TWARs/analyses/compare_to_dunnartPeaks/")

# genes with TWARs
twars_annot = annotatePeak("dunnart_twars_top_hits.bed", tssRegion = c(-3000, 3000), TxDb = smiCra_txdb)
twars_annot <- as.data.frame(twars_annot, row.names = NULL)
  
  # Convert refseq IDs and geneIDs to devil ensembl IDs and then to MouseIDs
  twars_annot$ensemblgeneID <- df2$Gene.stable.ID[match(unlist(twars_annot$geneId), df2$Gene.name)]
  twars_annot$ensemblgeneID = replace(twars_annot$ensemblgeneID,is.na(twars_annot$ensemblgeneID),"-")
  twars_annot$transcriptIdAltered <- gsub("\\..*","", twars_annot$transcriptId)
  twars_annot$refseqID <- df3$Ensembl.Gene.ID[match(unlist(twars_annot$transcriptIdAltered), df3$RefSeq.mRNA.Accession)]
  twars_annot$refseqID = replace(twars_annot$refseqID,is.na(twars_annot$refseqID),"-")
  twars_annot$combined <- ifelse(twars_annot$refseqID == "-", twars_annot$ensemblgeneID, twars_annot$refseqID)
  twars_annot$refseqID[twars_annot$refseqID == as.character("-")] <- NA
  twars_annot$mouseensembl <- df2$Mouse.gene.stable.ID[match(unlist(twars_annot$combined), df2$Gene.stable.ID)]
  twars_annot <- twars_annot[!is.na(twars_annot$mouseensembl),]

m = length(unique(twars_annot$combined))
# 189

# orthologous genes total
n = length(bg)
# 17304

# q = genes with dunnart either H3K4me3 or H3K27ac peak and TWAR
ac = fread("devil_twars/dunnart_twars_H3K27ac.bed")
me3 = fread("devil_twars/dunnart_twars_H3K4me3.bed")
overlap = rbind(ac, me3)
write.table(overlap, "dunnart_twars_H3K4me3_H3K27ac.bed", sep = "\t", quote=F, row.names=F, col.names = F)
overlap = annotatePeak("dunnart_twars_H3K4me3_H3K27ac.bed", tssRegion = c(-3000, 3000), TxDb = smiCra_txdb)
overlap <- as.data.frame(overlap, row.names = NULL)
  
  # Convert refseq IDs and geneIDs to devil ensembl IDs and then to MouseIDs
  overlap$ensemblgeneID <- df2$Gene.stable.ID[match(unlist(overlap$geneId), df2$Gene.name)]
  overlap$ensemblgeneID = replace(overlap$ensemblgeneID,is.na(overlap$ensemblgeneID),"-")
  overlap$transcriptIdAltered <- gsub("\\..*","", overlap$transcriptId)
  overlap$refseqID <- df3$Ensembl.Gene.ID[match(unlist(overlap$transcriptIdAltered), df3$RefSeq.mRNA.Accession)]
  overlap$refseqID = replace(overlap$refseqID,is.na(overlap$refseqID),"-")
  overlap$combined <- ifelse(overlap$refseqID == "-", overlap$ensemblgeneID, overlap$refseqID)
  overlap$refseqID[overlap$refseqID == as.character("-")] <- NA
  overlap$mouseensembl <- df2$Mouse.gene.stable.ID[match(unlist(overlap$combined), df2$Gene.stable.ID)]
  overlap <- overlap[!is.na(overlap$mouseensembl),]

q = length(unique(overlap$combined))

# genes with dunnart H3K4me3 or H3K27ac peaks
H3K27ac = annotatePeak("devil_twars/H3K27ac_overlap.narrowPeak", tssRegion = c(-3000, 3000), TxDb = smiCra_txdb)
H3K4me3 = annotatePeak("devil_twars/H3K4me3_overlap.narrowPeak", tssRegion = c(-3000, 3000), TxDb = smiCra_txdb)
H3K27ac <- as.data.frame(H3K27ac, row.names = NULL)
H3K4me3 <- as.data.frame(H3K4me3, row.names = NULL)

dunnart_genes_with_peaks = rbind(H3K27ac, H3K4me3)
dunnart_genes_with_peaks$ensemblgeneID <- df2$Gene.stable.ID[match(unlist(dunnart_genes_with_peaks$geneId), df2$Gene.name)]
  dunnart_genes_with_peaks$ensemblgeneID = replace(dunnart_genes_with_peaks$ensemblgeneID,is.na(dunnart_genes_with_peaks$ensemblgeneID),"-")
  dunnart_genes_with_peaks$transcriptIdAltered <- gsub("\\..*","", dunnart_genes_with_peaks$transcriptId)
  dunnart_genes_with_peaks$refseqID <- df3$Ensembl.Gene.ID[match(unlist(dunnart_genes_with_peaks$transcriptIdAltered), df3$RefSeq.mRNA.Accession)]
  dunnart_genes_with_peaks$refseqID = replace(dunnart_genes_with_peaks$refseqID,is.na(dunnart_genes_with_peaks$refseqID),"-")
  dunnart_genes_with_peaks$combined <- ifelse(dunnart_genes_with_peaks$refseqID == "-", dunnart_genes_with_peaks$ensemblgeneID, dunnart_genes_with_peaks$refseqID)
  dunnart_genes_with_peaks$refseqID[dunnart_genes_with_peaks$refseqID == as.character("-")] <- NA
  dunnart_genes_with_peaks$mouseensembl <- df2$Mouse.gene.stable.ID[match(unlist(dunnart_genes_with_peaks$combined), df2$Gene.stable.ID)]
  dunnart_genes_with_peaks <- dunnart_genes_with_peaks[!is.na(dunnart_genes_with_peaks$mouseensembl),]

k = length(unique(dunnart_genes_with_peaks$combined))

# q = genes with dunnart peak and TWAR
# m = genes with peaks
# n = genes with no peaks
# k = genes with TWARs

q = length(unique(overlap$combined)) # 91
m = length(unique(dunnart_genes_with_peaks$combined)) # 11910
n = length(bg) - length(unique(dunnart_genes_with_peaks$combined)) #5394
k = length(unique(twars_annot$combined)) #189

phyper(q =q, m = m, n = n, k = k)
# depleted

# q = genes with dunnart peak and TWAR 91
# m = genes with TWARs 189
# n = genes with no twar 17000
# k = genes with dunnart peaks that could have had a TWAR 11910

q = length(unique(overlap$combined)) # 91
m = length(unique(twars_annot$combined)) #189
n = length(unique(bg)) - length(unique(twars_annot$combined)) #17115
k = length(unique(dunnart_genes_with_peaks$combined)) # 11910
phyper(q =q, m = m, n = n, k = k, lower.tail=FALSE)
# depleted


# genes with dunnart promoter peaks
promoter = annotatePeak("cluster1_promoters.narrowPeak", tssRegion = c(-3000, 3000), TxDb = smiCra_txdb)
promoter <- as.data.frame(promoter, row.names = NULL)

dunnart_genes_with_peaks = copy(promoter)
dunnart_genes_with_peaks$ensemblgeneID <- df2$Gene.stable.ID[match(unlist(dunnart_genes_with_peaks$geneId), df2$Gene.name)]
  dunnart_genes_with_peaks$ensemblgeneID = replace(dunnart_genes_with_peaks$ensemblgeneID,is.na(dunnart_genes_with_peaks$ensemblgeneID),"-")
  dunnart_genes_with_peaks$transcriptIdAltered <- gsub("\\..*","", dunnart_genes_with_peaks$transcriptId)
  dunnart_genes_with_peaks$refseqID <- df3$Ensembl.Gene.ID[match(unlist(dunnart_genes_with_peaks$transcriptIdAltered), df3$RefSeq.mRNA.Accession)]
  dunnart_genes_with_peaks$refseqID = replace(dunnart_genes_with_peaks$refseqID,is.na(dunnart_genes_with_peaks$refseqID),"-")
  dunnart_genes_with_peaks$combined <- ifelse(dunnart_genes_with_peaks$refseqID == "-", dunnart_genes_with_peaks$ensemblgeneID, dunnart_genes_with_peaks$refseqID)
  dunnart_genes_with_peaks$refseqID[dunnart_genes_with_peaks$refseqID == as.character("-")] <- NA
  dunnart_genes_with_peaks$mouseensembl <- df2$Mouse.gene.stable.ID[match(unlist(dunnart_genes_with_peaks$combined), df2$Gene.stable.ID)]
  dunnart_genes_with_peaks <- dunnart_genes_with_peaks[!is.na(dunnart_genes_with_peaks$mouseensembl),]

k = length(unique(dunnart_genes_with_peaks$combined))
# 8530

# genes with dunnart promoter peak and TWAR
overlap = annotatePeak("dunnart_twars_promoters.bed", tssRegion = c(-3000, 3000), TxDb = smiCra_txdb)
overlap <- as.data.frame(overlap, row.names = NULL)
  
  # Convert refseq IDs and geneIDs to devil ensembl IDs and then to MouseIDs
  overlap$ensemblgeneID <- df2$Gene.stable.ID[match(unlist(overlap$geneId), df2$Gene.name)]
  overlap$ensemblgeneID = replace(overlap$ensemblgeneID,is.na(overlap$ensemblgeneID),"-")
  overlap$transcriptIdAltered <- gsub("\\..*","", overlap$transcriptId)
  overlap$refseqID <- df3$Ensembl.Gene.ID[match(unlist(overlap$transcriptIdAltered), df3$RefSeq.mRNA.Accession)]
  overlap$refseqID = replace(overlap$refseqID,is.na(overlap$refseqID),"-")
  overlap$combined <- ifelse(overlap$refseqID == "-", overlap$ensemblgeneID, overlap$refseqID)
  overlap$refseqID[overlap$refseqID == as.character("-")] <- NA
  overlap$mouseensembl <- df2$Mouse.gene.stable.ID[match(unlist(overlap$combined), df2$Gene.stable.ID)]
  overlap <- overlap[!is.na(overlap$mouseensembl),]

q = length(unique(overlap$combined))
q
# 35

# q = genes with promoter-peak and TWAR
# m = genes with promoter-peaks
# n = genes with no promoter-peaks
# k = genes with TWARs

q = length(unique(overlap$combined)) # 35
m = length(unique(dunnart_genes_with_peaks$combined)) # 8530
n = length(bg) - length(unique(dunnart_genes_with_peaks$combined)) # 8774
k = length(unique(twars_annot$combined)) #189

phyper(q =q, m = m, n = n, k = k)
# depleted

# genes with dunnart enhancer peaks
enhancer = annotatePeak("dunnart_enhancers.narrowPeak", tssRegion = c(-3000, 3000), TxDb = smiCra_txdb)
enhancer <- as.data.frame(enhancer, row.names = NULL)

dunnart_genes_with_peaks = copy(enhancer)
dunnart_genes_with_peaks$ensemblgeneID <- df2$Gene.stable.ID[match(unlist(dunnart_genes_with_peaks$geneId), df2$Gene.name)]
  dunnart_genes_with_peaks$ensemblgeneID = replace(dunnart_genes_with_peaks$ensemblgeneID,is.na(dunnart_genes_with_peaks$ensemblgeneID),"-")
  dunnart_genes_with_peaks$transcriptIdAltered <- gsub("\\..*","", dunnart_genes_with_peaks$transcriptId)
  dunnart_genes_with_peaks$refseqID <- df3$Ensembl.Gene.ID[match(unlist(dunnart_genes_with_peaks$transcriptIdAltered), df3$RefSeq.mRNA.Accession)]
  dunnart_genes_with_peaks$refseqID = replace(dunnart_genes_with_peaks$refseqID,is.na(dunnart_genes_with_peaks$refseqID),"-")
  dunnart_genes_with_peaks$combined <- ifelse(dunnart_genes_with_peaks$refseqID == "-", dunnart_genes_with_peaks$ensemblgeneID, dunnart_genes_with_peaks$refseqID)
  dunnart_genes_with_peaks$refseqID[dunnart_genes_with_peaks$refseqID == as.character("-")] <- NA
  dunnart_genes_with_peaks$mouseensembl <- df2$Mouse.gene.stable.ID[match(unlist(dunnart_genes_with_peaks$combined), df2$Gene.stable.ID)]
  dunnart_genes_with_peaks <- dunnart_genes_with_peaks[!is.na(dunnart_genes_with_peaks$mouseensembl),]

k = length(unique(dunnart_genes_with_peaks$combined))
# 8850

# genes with dunnart enhancer peak and twar
overlap = annotatePeak("dunnart_twars_enhancers.bed", tssRegion = c(-3000, 3000), TxDb = smiCra_txdb)
overlap <- as.data.frame(overlap, row.names = NULL)
  
  # Convert refseq IDs and geneIDs to devil ensembl IDs and then to MouseIDs
  overlap$ensemblgeneID <- df2$Gene.stable.ID[match(unlist(overlap$geneId), df2$Gene.name)]
  overlap$ensemblgeneID = replace(overlap$ensemblgeneID,is.na(overlap$ensemblgeneID),"-")
  overlap$transcriptIdAltered <- gsub("\\..*","", overlap$transcriptId)
  overlap$refseqID <- df3$Ensembl.Gene.ID[match(unlist(overlap$transcriptIdAltered), df3$RefSeq.mRNA.Accession)]
  overlap$refseqID = replace(overlap$refseqID,is.na(overlap$refseqID),"-")
  overlap$combined <- ifelse(overlap$refseqID == "-", overlap$ensemblgeneID, overlap$refseqID)
  overlap$refseqID[overlap$refseqID == as.character("-")] <- NA
  overlap$mouseensembl <- df2$Mouse.gene.stable.ID[match(unlist(overlap$combined), df2$Gene.stable.ID)]
  overlap <- overlap[!is.na(overlap$mouseensembl),]

q = length(unique(overlap$combined))
# 18
# q = genes with enhancer-peak and TWAR
# m = genes with enhancer-peaks
# n = genes with no enhancer-peaks
# k = genes with TWARs

q = length(unique(overlap$combined)) # 18
m = length(unique(dunnart_genes_with_peaks$combined)) # 8850
n = length(bg) - length(unique(dunnart_genes_with_peaks$combined)) # 8454
k = length(unique(twars_annot$combined)) #189

phyper(q =q, m = m, n = n, k = k)
# depleted

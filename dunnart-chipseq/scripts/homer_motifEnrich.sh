## Laura Cook
## Motif enrichment with HOMER

# installed HOMER with the following modules loaded
module load gcc
module load perl
module load web_proxy
module load wget/1.20.1


## Load genome for use with HOMER tools
## This will allow the genome to be called within the tools
loadGenome.pl -name smiCra1 -org null -fasta /data/projects/punim0586/lecook/chipseq-pipeline/cross_species/data/genomes/Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr.fasta -gff Scras_dunnart_assem1.0_pb-ont-illsr_flyeassem_red-rd-scfitr2_pil2xwgs2_60chr2.gff

## analyse genomic regions for de novo and known motifs
## find enrichment compared to a random expectation based on 
## default fragment size for motif finding is 200bp
findMotifsGenome.pl dunnart_enhancers.narrowPeak smiCra1 motifEnrichment/enhancers/ -size given -p 32 -preparse -mknown /data/projects/punim0586/lecook/chipseq-pipeline/cross_species/bin/homer/data/knownTFs/vertebrates/known.motifs

findMotifsGenome.pl dunnart_promoters.narrowPeak smiCra1 motifEnrichment/promoters/ -size given -p 32 -preparse -mknown /data/projects/punim0586/lecook/chipseq-pipeline/cross_species/bin/homer/data/knownTFs/vertebrates/known.motifs

findMotifsGenome.pl cluster1_promoters.narrowPeak smiCra1 motifEnrichment/cluster1_promoters/ -size given  -p 32 -preparse -mknown /data/projects/punim0586/lecook/chipseq-pipeline/cross_species/bin/homer/data/knownTFs/vertebrates/known.motifs

findMotifsGenome.pl cluster1_promoters.narrowPeak smiCra1 motifEnrichment/cluster1_promoters_polII/ -size given -mknown /data/projects/punim0586/lecook/chipseq-pipeline/cross_species/bin/homer/data/knownTFs/motifs/groups/promoter.motifs  -p 32 -preparse

findMotifsGenome.pl dunnart_enhancers.narrowPeak smiCra1 motifEnrichment/enhancers_polII/ -size given -mknown /data/projects/punim0586/lecook/chipseq-pipeline/cross_species/bin/homer/data/knownTFs/motifs/groups/promoter.motifs  -p 32 -preparse

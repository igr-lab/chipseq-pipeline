# List of liftOver and bedtools commands to compare peaks in the mouse and dunnart
# using liftOver chains 

module load bedtools/2.30.0
module load ucsc


###################################################################################################
###################################### COMPARING PEAKS ############################################
###################################################################################################

## All peaks
less enhancers/dunnart_enhancer_peaks.narrowPeak | awk '{print $1,$2,$3}' > dunnart.enhancer_peaks_coord.bed
less promoters/clustered/dunnart.clustered.narrowPeak | awk '{print $1,$2,$3}' > promoters/clustered/dunnart.promoter_peaks_coord.bed

liftOver enhancers/dunnart.enhancer_peaks_coord.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain dunnart.enhancer_smiCraTOmm10.bed enhancer_smiCraTOmm10_unmapped.bed
liftOver promoters/clustered/dunnart.promoter_peaks_coord.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain dunnart.promoter_smiCraTOmm10.bed promoter_smiCraTOmm10_unmapped.bed
# enhancer = 437 peaks
# promoter = 48 peaks

# Import output files in R
enh = fread("enhancers/dunnart.enhancer_smiCraTOmm10.bed")
prom = fread("promoters/clustered/dunnart.promoter_smiCraTOmm10.bed")
data = list(enh, prom)
names(data) = c("enhancers", "promoters")
data = Map(mutate, data, cre = names(data))
data = lapply(data, function(x) x[order(V4),])
data = lapply(data, function(x) x=x[,width:=V3-V2])
data = rbindlist(data,)


###################################################################################################
################################### EXTRACT PEAK SUMMITS ########################################
###################################################################################################

# make list of peaks enhancer and promoter groups
less enhancers/dunnart_enhancer_peaks.narrowPeak | awk -F '\t' '{print $4}' | sort > enhancers/enhancer_peak_IDs.txt
less promoters/clustered/dunnart.clustered.narrowPeak | awk -F '\t' '{print $4}' | sort > promoters/clustered/promoter_peak_IDs.txt

cat /data/projects/punim0586/lecook/chipseq-pipeline/dunnart/results_10M/macs2/H3K4me3_pooled_macs2_summits.bed /data/projects/punim0586/lecook/chipseq-pipeline/dunnart/results_10M/macs2/H3K27ac_pooled_macs2_summits.bed > H3K4me3_H3K27ac_combinedSummits.bed

less E10.5_enhancer_peaks.narrowPeak | awk -F '\t' '{print $4}'  > E10.5_enhancer_peaks_IDs.txt
less E11.5_enhancer_peaks.narrowPeak | awk -F '\t' '{print $4}'  > E11.5_enhancer_peaks_IDs.txt
less E12.5_enhancer_peaks.narrowPeak | awk -F '\t' '{print $4}'  > E12.5_enhancer_peaks_IDs.txt
less E13.5_enhancer_peaks.narrowPeak | awk -F '\t' '{print $4}'  > E13.5_enhancer_peaks_IDs.txt
less E14.5_enhancer_peaks.narrowPeak | awk -F '\t' '{print $4}'  > E14.5_enhancer_peaks_IDs.txt
less E15.5_enhancer_peaks.narrowPeak | awk -F '\t' '{print $4}'  > E15.5_enhancer_peaks_IDs.txt

less  E10.5.clustered.narrowPeak | awk -F '\t' '{print $4}' > promoters/clustered/E10.5_promoter_peaks_IDs.txt
less  E11.5.clustered.narrowPeak | awk -F '\t' '{print $4}'  > promoters/clustered/E11.5_promoter_peaks_IDs.txt
less  E12.5.clustered.narrowPeak | awk -F '\t' '{print $4}'  > promoters/clustered/E12.5_promoter_peaks_IDs.txt
less  E13.5.clustered.narrowPeak | awk -F '\t' '{print $4}'  > promoters/clustered/E13.5_promoter_peaks_IDs.txt
less  E14.5.clustered.narrowPeak | awk -F '\t' '{print $4}'  > promoters/clustered/E14.5_promoter_peaks_IDs.txt
less  E15.5.clustered.narrowPeak | awk -F '\t' '{print $4}'  > promoters/clustered/E15.5_promoter_peaks_IDs.txt

setwd("/data/projects/punim0586/lecook/chipseq-pipeline/mouse/results_10M/macs2")
cat E10.5_H3K27ac.pooled.macs2_summits.bed E10.5_H3K4me3.pooled.macs2_summits.bed > ../../../cross_species/peakAnnotation/consensus/E10.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed
cat E11.5_H3K27ac.pooled.macs2_summits.bed E11.5_H3K4me3.pooled.macs2_summits.bed > ../../../cross_species/peakAnnotation/consensus/E11.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed
cat E12.5_H3K27ac.pooled.macs2_summits.bed E12.5_H3K4me3.pooled.macs2_summits.bed > ../../../cross_species/peakAnnotation/consensus/E12.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed
cat E13.5_H3K27ac.pooled.macs2_summits.bed E13.5_H3K4me3.pooled.macs2_summits.bed > ../../../cross_species/peakAnnotation/consensus/E13.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed
cat E14.5_H3K27ac.pooled.macs2_summits.bed E14.5_H3K4me3.pooled.macs2_summits.bed > ../../../cross_species/peakAnnotation/consensus/E14.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed
cat E15.5_H3K27ac.pooled.macs2_summits.bed E15.5_H3K4me3.pooled.macs2_summits.bed > ../../../cross_species/peakAnnotation/consensus/E15.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed

# in R extract rows from summits file based on the list of peaks in overlap file for both enhancers and promoters
## Dunnart
summits <- read.table("H3K4me3_H3K27ac_combinedSummits.bed")
reps1 <- scan("enhancers/enhancer_peak_IDs.txt", what="", sep="\n")
reps2 <- scan("promoters/clustered/promoter_peak_IDs.txt", what="", sep="\n")
enh_summits <- subset(summits, V4 %in% reps1)
pro_summits <- subset(summits, V4 %in% reps2)
write.table(enh_summits, "dunnart.enhancer_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(pro_summits, "dunnart.promoter_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")

## Mouse
setwd("/data/projects/punim0586/lecook/chipseq-pipeline/cross_species/peakAnnotation/consensus")
E10 <- read.table("E10.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed")
E11<- read.table("E11.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed")
E12<- read.table("E12.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed")
E13<- read.table("E13.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed")
E14<- read.table("E14.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed")
E15<- read.table("E15.5_H3K27ac_H3K4me3.pooled.macs2_summits.bed")

p10 <- scan("promoters/clustered/E10.5_promoter_peaks_IDs.txt", what="", sep="\n")
p11 <- scan("promoters/clustered/E11.5_promoter_peaks_IDs.txt", what="", sep="\n")
p12 <- scan("promoters/clustered/E12.5_promoter_peaks_IDs.txt", what="", sep="\n")
p13 <- scan("promoters/clustered/E13.5_promoter_peaks_IDs.txt", what="", sep="\n")
p14 <- scan("promoters/clustered/E14.5_promoter_peaks_IDs.txt", what="", sep="\n")
p15 <- scan("promoters/clustered/E15.5_promoter_peaks_IDs.txt", what="", sep="\n")

e10 <- scan("enhancers/E10.5_enhancer_peaks_IDs.txt", what="", sep="\n")
e11 <- scan("enhancers/E11.5_enhancer_peaks_IDs.txt", what="", sep="\n")
e12 <- scan("enhancers/E12.5_enhancer_peaks_IDs.txt", what="", sep="\n")
e13 <- scan("enhancers/E13.5_enhancer_peaks_IDs.txt", what="", sep="\n")
e14 <- scan("enhancers/E14.5_enhancer_peaks_IDs.txt", what="", sep="\n")
e15 <- scan("enhancers/E15.5_enhancer_peaks_IDs.txt", what="", sep="\n")

write.table(subset(E10, V4 %in% p10), "promoters/clustered/E10_promoter_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E11, V4 %in% p11), "promoters/clustered/E11_promoter_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E12, V4 %in% p12), "promoters/clustered/E12_promoter_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E13, V4 %in% p13), "promoters/clustered/E13_promoter_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E14, V4 %in% p14), "promoters/clustered/E14_promoter_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E15, V4 %in% p15), "promoters/clustered/E15_promoter_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")

write.table(subset(E10, V4 %in% e10), "enhancers/E10_enhancer_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E11, V4 %in% e11), "enhancers/E11_enhancer_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E12, V4 %in% e12), "enhancers/E12_enhancer_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E13, V4 %in% e13), "enhancers/E13_enhancer_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E14, V4 %in% e14), "enhancers/E14_enhancer_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
write.table(subset(E15, V4 %in% e15), "enhancers/E15_enhancer_overlap_summits.bed", quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")


# in bash use bedtools to extend the summits 25bp in both directions to have 50bp 
bedtools slop -i enhancers/dunnart.enhancer_overlap_summits.bed -b 25 -g smiCra1.chrom.sizes > enhancers/enhancer_overlap_50bpsummits.bed
bedtools slop -i promoters/clustered/dunnart.promoter_overlap_summits.bed -b 25 -g smiCra1.chrom.sizes > promoters/clustered/promoter_overlap_50bpsummits.bed

bedtools slop -i enhancers/E10_enhancer_overlap_summits.bed -b 25 -g mm10.chrom.sizes > enhancers/E10_enhancer_overlap_50bpsummits.bed
bedtools slop -i enhancers/E11_enhancer_overlap_summits.bed -b 25 -g mm10.chrom.sizes > enhancers/E11_enhancer_overlap_50bpsummits.bed
bedtools slop -i enhancers/E12_enhancer_overlap_summits.bed -b 25 -g mm10.chrom.sizes > enhancers/E12_enhancer_overlap_50bpsummits.bed
bedtools slop -i enhancers/E13_enhancer_overlap_summits.bed -b 25 -g mm10.chrom.sizes > enhancers/E13_enhancer_overlap_50bpsummits.bed
bedtools slop -i enhancers/E14_enhancer_overlap_summits.bed -b 25 -g mm10.chrom.sizes > enhancers/E14_enhancer_overlap_50bpsummits.bed
bedtools slop -i enhancers/E15_enhancer_overlap_summits.bed -b 25 -g mm10.chrom.sizes > enhancers/E15_enhancer_overlap_50bpsummits.bed

bedtools slop -i promoters/clustered/E10_promoter_overlap_summits.bed -b 25 -g mm10.chrom.sizes > promoters/clustered/E10_promoter_overlap_50bpsummits.bed
bedtools slop -i promoters/clustered/E11_promoter_overlap_summits.bed -b 25 -g mm10.chrom.sizes > promoters/clustered/E11_promoter_overlap_50bpsummits.bed
bedtools slop -i promoters/clustered/E12_promoter_overlap_summits.bed -b 25 -g mm10.chrom.sizes > promoters/clustered/E12_promoter_overlap_50bpsummits.bed
bedtools slop -i promoters/clustered/E13_promoter_overlap_summits.bed -b 25 -g mm10.chrom.sizes > promoters/clustered/E13_promoter_overlap_50bpsummits.bed
bedtools slop -i promoters/clustered/E14_promoter_overlap_summits.bed -b 25 -g mm10.chrom.sizes > promoters/clustered/E14_promoter_overlap_50bpsummits.bed
bedtools slop -i promoters/clustered/E15_promoter_overlap_summits.bed -b 25 -g mm10.chrom.sizes > promoters/clustered/E15_promoter_overlap_50bpsummits.bed

# in bash use bedtools to extend the summits 50bp in both directions to have 100bp 
bedtools slop -i enhancers/dunnart.enhancer_overlap_summits.bed -b 50 -g smiCra1.chrom.sizes > enhancers/enhancer_overlap_100bpsummits.bed
bedtools slop -i promoters/dunnart.promoter_overlap_summits.bed -b 50 -g smiCra1.chrom.sizes > promoters/promoter_overlap_100bpsummits.bed

bedtools slop -i enhancers/E10_enhancer_overlap_summits.bed -b 50 -g mm10.chrom.sizes > enhancers/E10_enhancer_overlap_100bpsummits.bed
bedtools slop -i enhancers/E11_enhancer_overlap_summits.bed -b 50 -g mm10.chrom.sizes > enhancers/E11_enhancer_overlap_100bpsummits.bed
bedtools slop -i enhancers/E12_enhancer_overlap_summits.bed -b 50 -g mm10.chrom.sizes > enhancers/E12_enhancer_overlap_100bpsummits.bed
bedtools slop -i enhancers/E13_enhancer_overlap_summits.bed -b 50 -g mm10.chrom.sizes > enhancers/E13_enhancer_overlap_100bpsummits.bed
bedtools slop -i enhancers/E14_enhancer_overlap_summits.bed -b 50 -g mm10.chrom.sizes > enhancers/E14_enhancer_overlap_100bpsummits.bed
bedtools slop -i enhancers/E15_enhancer_overlap_summits.bed -b 50 -g mm10.chrom.sizes > enhancers/E15_enhancer_overlap_100bpsummits.bed

bedtools slop -i promoters/E10_promoter_overlap_summits.bed -b 50 -g mm10.chrom.sizes > promoters/E10_promoter_overlap_100bpsummits.bed
bedtools slop -i promoters/E11_promoter_overlap_summits.bed -b 50 -g mm10.chrom.sizes > promoters/E11_promoter_overlap_100bpsummits.bed
bedtools slop -i promoters/E12_promoter_overlap_summits.bed -b 50 -g mm10.chrom.sizes > promoters/E12_promoter_overlap_100bpsummits.bed
bedtools slop -i promoters/E13_promoter_overlap_summits.bed -b 50 -g mm10.chrom.sizes > promoters/E13_promoter_overlap_100bpsummits.bed
bedtools slop -i promoters/E14_promoter_overlap_summits.bed -b 50 -g mm10.chrom.sizes > promoters/E14_promoter_overlap_100bpsummits.bed
bedtools slop -i promoters/E15_promoter_overlap_summits.bed -b 50 -g mm10.chrom.sizes > promoters/E15_promoter_overlap_100bpsummits.bed

# in bash use bedtools to extend the summits 100bp in both directions to have 200bp 
bedtools slop -i enhancers/dunnart.enhancer_overlap_summits.bed -b 100 -g smiCra1.chrom.sizes > enhancers/enhancer_overlap_200bpsummits.bed
bedtools slop -i promoters/dunnart.promoter_overlap_summits.bed -b 100 -g smiCra1.chrom.sizes > promoters/promoter_overlap_200bpsummits.bed

bedtools slop -i enhancers/E10_enhancer_overlap_summits.bed -b 100 -g mm10.chrom.sizes > enhancers/E10_enhancer_overlap_200bpsummits.bed
bedtools slop -i enhancers/E11_enhancer_overlap_summits.bed -b 100 -g mm10.chrom.sizes > enhancers/E11_enhancer_overlap_200bpsummits.bed
bedtools slop -i enhancers/E12_enhancer_overlap_summits.bed -b 100 -g mm10.chrom.sizes > enhancers/E12_enhancer_overlap_200bpsummits.bed
bedtools slop -i enhancers/E13_enhancer_overlap_summits.bed -b 100 -g mm10.chrom.sizes > enhancers/E13_enhancer_overlap_200bpsummits.bed
bedtools slop -i enhancers/E14_enhancer_overlap_summits.bed -b 100 -g mm10.chrom.sizes > enhancers/E14_enhancer_overlap_200bpsummits.bed
bedtools slop -i enhancers/E15_enhancer_overlap_summits.bed -b 100 -g mm10.chrom.sizes > enhancers/E15_enhancer_overlap_200bpsummits.bed

bedtools slop -i promoters/E10_promoter_overlap_summits.bed -b 100 -g mm10.chrom.sizes > promoters/E10_promoter_overlap_200bpsummits.bed
bedtools slop -i promoters/E11_promoter_overlap_summits.bed -b 100 -g mm10.chrom.sizes > promoters/E11_promoter_overlap_200bpsummits.bed
bedtools slop -i promoters/E12_promoter_overlap_summits.bed -b 100 -g mm10.chrom.sizes > promoters/E12_promoter_overlap_200bpsummits.bed
bedtools slop -i promoters/E13_promoter_overlap_summits.bed -b 100 -g mm10.chrom.sizes > promoters/E13_promoter_overlap_200bpsummits.bed
bedtools slop -i promoters/E14_promoter_overlap_summits.bed -b 100 -g mm10.chrom.sizes > promoters/E14_promoter_overlap_200bpsummits.bed
bedtools slop -i promoters/E15_promoter_overlap_summits.bed -b 100 -g mm10.chrom.sizes > promoters/E15_promoter_overlap_200bpsummits.bed

bedtools slop -i enhancers/dunnart.enhancer_overlap_summits.bed -b 150 -g smiCra1.chrom.sizes > enhancers/enhancer_overlap_300bpsummits.bed
bedtools slop -i promoters/dunnart.promoter_overlap_summits.bed -b 150 -g smiCra1.chrom.sizes > promoters/promoter_overlap_300bpsummits.bed

bedtools slop -i enhancers/E10_enhancer_overlap_summits.bed -b 150 -g mm10.chrom.sizes > enhancers/E10_enhancer_overlap_300bpsummits.bed
bedtools slop -i enhancers/E11_enhancer_overlap_summits.bed -b 150 -g mm10.chrom.sizes > enhancers/E11_enhancer_overlap_300bpsummits.bed
bedtools slop -i enhancers/E12_enhancer_overlap_summits.bed -b 150 -g mm10.chrom.sizes > enhancers/E12_enhancer_overlap_300bpsummits.bed
bedtools slop -i enhancers/E13_enhancer_overlap_summits.bed -b 150 -g mm10.chrom.sizes > enhancers/E13_enhancer_overlap_300bpsummits.bed
bedtools slop -i enhancers/E14_enhancer_overlap_summits.bed -b 150 -g mm10.chrom.sizes > enhancers/E14_enhancer_overlap_300bpsummits.bed
bedtools slop -i enhancers/E15_enhancer_overlap_summits.bed -b 150 -g mm10.chrom.sizes > enhancers/E15_enhancer_overlap_300bpsummits.bed

bedtools slop -i promoters/E10_promoter_overlap_summits.bed -b 150 -g mm10.chrom.sizes > promoters/E10_promoter_overlap_300bpsummits.bed
bedtools slop -i promoters/E11_promoter_overlap_summits.bed -b 150 -g mm10.chrom.sizes > promoters/E11_promoter_overlap_300bpsummits.bed
bedtools slop -i promoters/E12_promoter_overlap_summits.bed -b 150 -g mm10.chrom.sizes > promoters/E12_promoter_overlap_300bpsummits.bed
bedtools slop -i promoters/E13_promoter_overlap_summits.bed -b 150 -g mm10.chrom.sizes > promoters/E13_promoter_overlap_300bpsummits.bed
bedtools slop -i promoters/E14_promoter_overlap_summits.bed -b 150 -g mm10.chrom.sizes > promoters/E14_promoter_overlap_300bpsummits.bed
bedtools slop -i promoters/E15_promoter_overlap_summits.bed -b 150 -g mm10.chrom.sizes > promoters/E15_promoter_overlap_300bpsummits.bed

bedtools slop -i enhancers/dunnart.enhancer_overlap_summits.bed -b 200 -g smiCra1.chrom.sizes > enhancers/enhancer_overlap_400bpsummits.bed
bedtools slop -i promoters/dunnart.promoter_overlap_summits.bed -b 200 -g smiCra1.chrom.sizes > promoters/promoter_overlap_400bpsummits.bed

bedtools slop -i enhancers/E10_enhancer_overlap_summits.bed -b 200 -g mm10.chrom.sizes > enhancers/E10_enhancer_overlap_400bpsummits.bed
bedtools slop -i enhancers/E11_enhancer_overlap_summits.bed -b 200 -g mm10.chrom.sizes > enhancers/E11_enhancer_overlap_400bpsummits.bed
bedtools slop -i enhancers/E12_enhancer_overlap_summits.bed -b 200 -g mm10.chrom.sizes > enhancers/E12_enhancer_overlap_400bpsummits.bed
bedtools slop -i enhancers/E13_enhancer_overlap_summits.bed -b 200 -g mm10.chrom.sizes > enhancers/E13_enhancer_overlap_400bpsummits.bed
bedtools slop -i enhancers/E14_enhancer_overlap_summits.bed -b 200 -g mm10.chrom.sizes > enhancers/E14_enhancer_overlap_400bpsummits.bed
bedtools slop -i enhancers/E15_enhancer_overlap_summits.bed -b 200 -g mm10.chrom.sizes > enhancers/E15_enhancer_overlap_400bpsummits.bed

bedtools slop -i promoters/E10_promoter_overlap_summits.bed -b 200 -g mm10.chrom.sizes > promoters/E10_promoter_overlap_400bpsummits.bed
bedtools slop -i promoters/E11_promoter_overlap_summits.bed -b 200 -g mm10.chrom.sizes > promoters/E11_promoter_overlap_400bpsummits.bed
bedtools slop -i promoters/E12_promoter_overlap_summits.bed -b 200 -g mm10.chrom.sizes > promoters/E12_promoter_overlap_400bpsummits.bed
bedtools slop -i promoters/E13_promoter_overlap_summits.bed -b 200 -g mm10.chrom.sizes > promoters/E13_promoter_overlap_400bpsummits.bed
bedtools slop -i promoters/E14_promoter_overlap_summits.bed -b 200 -g mm10.chrom.sizes > promoters/E14_promoter_overlap_400bpsummits.bed
bedtools slop -i promoters/E15_promoter_overlap_summits.bed -b 200 -g mm10.chrom.sizes > promoters/E15_promoter_overlap_400bpsummits.bed

bedtools slop -i enhancers/dunnart.enhancer_overlap_summits.bed -b 250 -g smiCra1.chrom.sizes > enhancers/enhancer_overlap_500bpsummits.bed
bedtools slop -i promoters/dunnart.promoter_overlap_summits.bed -b 250 -g smiCra1.chrom.sizes > promoters/promoter_overlap_500bpsummits.bed

bedtools slop -i enhancers/E10_enhancer_overlap_summits.bed -b 250 -g mm10.chrom.sizes > enhancers/E10_enhancer_overlap_500bpsummits.bed
bedtools slop -i enhancers/E11_enhancer_overlap_summits.bed -b 250 -g mm10.chrom.sizes > enhancers/E11_enhancer_overlap_500bpsummits.bed
bedtools slop -i enhancers/E12_enhancer_overlap_summits.bed -b 250 -g mm10.chrom.sizes > enhancers/E12_enhancer_overlap_500bpsummits.bed
bedtools slop -i enhancers/E13_enhancer_overlap_summits.bed -b 250 -g mm10.chrom.sizes > enhancers/E13_enhancer_overlap_500bpsummits.bed
bedtools slop -i enhancers/E14_enhancer_overlap_summits.bed -b 250 -g mm10.chrom.sizes > enhancers/E14_enhancer_overlap_500bpsummits.bed
bedtools slop -i enhancers/E15_enhancer_overlap_summits.bed -b 250 -g mm10.chrom.sizes > enhancers/E15_enhancer_overlap_500bpsummits.bed

bedtools slop -i promoters/E10_promoter_overlap_summits.bed -b 250 -g mm10.chrom.sizes > promoters/E10_promoter_overlap_500bpsummits.bed
bedtools slop -i promoters/E11_promoter_overlap_summits.bed -b 250 -g mm10.chrom.sizes > promoters/E11_promoter_overlap_500bpsummits.bed
bedtools slop -i promoters/E12_promoter_overlap_summits.bed -b 250 -g mm10.chrom.sizes > promoters/E12_promoter_overlap_500bpsummits.bed
bedtools slop -i promoters/E13_promoter_overlap_summits.bed -b 250 -g mm10.chrom.sizes > promoters/E13_promoter_overlap_500bpsummits.bed
bedtools slop -i promoters/E14_promoter_overlap_summits.bed -b 250 -g mm10.chrom.sizes > promoters/E14_promoter_overlap_500bpsummits.bed
bedtools slop -i promoters/E15_promoter_overlap_summits.bed -b 250 -g mm10.chrom.sizes > promoters/E15_promoter_overlap_500bpsummits.bed


###################################################################################################
################################### LIFTOVER PEAK SUMMITS ########################################
###################################################################################################

#### COMBINED PEAKS - ACTIVE IN BOTH DUNNART AND MOUSE ####

# LiftOver dunnart peaks to mouse genome
liftOver enhancers/enhancer_overlap_50bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit50bp_smiCraTOmm10.bed enhancers/enhancer_summit50bp_smiCraTOmm10_unmapped.bed
liftOver enhancers/enhancer_overlap_100bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit100bp_smiCraTOmm10.bed enhancers/enhancer_summit100bp_smiCraTOmm10_unmapped.bed
liftOver enhancers/enhancer_overlap_200bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit200bp_smiCraTOmm10.bed enhancers/enhancer_summit200bp_smiCraTOmm10_unmapped.bed
liftOver enhancers/enhancer_overlap_300bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit300bp_smiCraTOmm10.bed enhancers/enhancer_summit300bp_smiCraTOmm10_unmapped.bed
liftOver enhancers/enhancer_overlap_400bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit400bp_smiCraTOmm10.bed enhancers/enhancer_summit400bp_smiCraTOmm10_unmapped.bed
liftOver enhancers/enhancer_overlap_500bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit500bp_smiCraTOmm10.bed enhancers/enhancer_summit500bp_smiCraTOmm10_unmapped.bed

liftOver promoters/clustered/promoter_overlap_50bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/clustered/promoter_summit50bp_smiCraTOmm10.bed promoters/clustered/promoter_summit50bp_smiCraTOmm10_unmapped.bed
liftOver promoters/promoter_overlap_100bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit100bp_smiCraTOmm10.bed promoters/promoter_summit100bp_smiCraTOmm10_unmapped.bed
liftOver promoters/promoter_overlap_200bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit200bp_smiCraTOmm10.bed promoters/promoter_summit200bp_smiCraTOmm10_unmapped.bed
liftOver promoters/promoter_overlap_300bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit300bp_smiCraTOmm10.bed promoters/promoter_summit300bp_smiCraTOmm10_unmapped.bed
liftOver promoters/promoter_overlap_400bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit400bp_smiCraTOmm10.bed promoters/promoter_summit400bp_smiCraTOmm10_unmapped.bed
liftOver promoters/promoter_overlap_500bpsummits.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit500bp_smiCraTOmm10.bed promoters/promoter_summit500bp_smiCraTOmm10_unmapped.bed

## Lift back to the dunnart genome
liftOver enhancers/enhancer_summit50bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit50bp_smiCraTOmm10_mm10TOsmiCra.bed enhancers/enhancer_summit50bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver enhancers/enhancer_summit100bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit100bp_smiCraTOmm10_mm10TOsmiCra.bed enhancers/enhancer_summit100bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver enhancers/enhancer_summit200bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit200bp_smiCraTOmm10_mm10TOsmiCra.bed enhancers/enhancer_summit200bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver enhancers/enhancer_summit300bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit300bp_smiCraTOmm10_mm10TOsmiCra.bed enhancers/enhancer_summit300bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver enhancers/enhancer_summit400bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit400bp_smiCraTOmm10_mm10TOsmiCra.bed enhancers/enhancer_summit400bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver enhancers/enhancer_summit500bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit500bp_smiCraTOmm10_mm10TOsmiCra.bed enhancers/enhancer_summit500bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
# 3820
liftOver promoters/promoter_summit50bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit50bp_smiCraTOmm10_mm10TOsmiCra.bed promoters/promoter_summit50bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver promoters/promoter_summit100bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit100bp_smiCraTOmm10_mm10TOsmiCra.bed promoters/promoter_summit100bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver promoters/promoter_summit200bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit200bp_smiCraTOmm10_mm10TOsmiCra.bed promoters/promoter_summit200bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver promoters/promoter_summit300bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit300bp_smiCraTOmm10_mm10TOsmiCra.bed promoters/promoter_summit300bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver promoters/promoter_summit400bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit400bp_smiCraTOmm10_mm10TOsmiCra.bed promoters/promoter_summit400bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver promoters/promoter_summit500bp_smiCraTOmm10.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit500bp_smiCraTOmm10_mm10TOsmiCra.bed promoters/promoter_summit500bp_smiCraTOmm10_mm10TOsmiCra_unmapped.bed

# Lift back to mouse genome so that everything is in mouse coordinates
liftOver enhancers/enhancer_summit50bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit50bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed enhancers/enhancer_summit50bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver enhancers/enhancer_summit100bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit100bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed enhancers/enhancer_summit100bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver enhancers/enhancer_summit200bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit200bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed enhancers/enhancer_summit200bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver enhancers/enhancer_summit300bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit300bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed enhancers/enhancer_summit300bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver enhancers/enhancer_summit400bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit400bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed enhancers/enhancer_summit400bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver enhancers/enhancer_summit500bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/enhancer_summit500bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed enhancers/enhancer_summit500bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed

# 3820
liftOver promoters/promoter_summit50bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit50bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed promoters/promoter_summit50bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver promoters/promoter_summit100bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit100bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed promoters/promoter_summit100bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver promoters/promoter_summit200bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit200bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed promoters/promoter_summit200bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver promoters/promoter_summit300bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit300bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed promoters/promoter_summit300bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver promoters/promoter_summit400bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit400bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed promoters/promoter_summit400bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed
liftOver promoters/promoter_summit500bp_smiCraTOmm10_mm10TOsmiCra.bed ../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/promoter_summit500bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10.bed promoters/promoter_summit500bp_smiCraTOmm10_mm10TOsmiCra_backTOmm10_unmapped.bed

# 1881
## Intersect dunnart CREs with mouse CREs.
bedtools intersect -a promoters/promoter_summit50bp_smiCraTOmm10.bed -b promoters/E10.5.clustered.narrowPeak -wo > promoters/E10.5_promoterOverlapmm10_50bpsummits.bed
bedtools intersect -a promoters/promoter_summit50bp_smiCraTOmm10.bed -b promoters/E11.5.clustered.narrowPeak -wo > promoters/E11.5_promoterOverlapmm10_50bpsummits.bed
bedtools intersect -a promoters/promoter_summit50bp_smiCraTOmm10.bed -b promoters/E12.5.clustered.narrowPeak -wo > promoters/E12.5_promoterOverlapmm10_50bpsummits.bed
bedtools intersect -a promoters/promoter_summit50bp_smiCraTOmm10.bed -b promoters/E13.5.clustered.narrowPeak -wo > promoters/E13.5_promoterOverlapmm10_50bpsummits.bed
bedtools intersect -a promoters/promoter_summit50bp_smiCraTOmm10.bed -b promoters/E14.5.clustered.narrowPeak -wo > promoters/E14.5_promoterOverlapmm10_50bpsummits.bed
bedtools intersect -a promoters/promoter_summit50bp_smiCraTOmm10.bed -b promoters/E15.5.clustered.narrowPeak -wo > promoters/E15.5_promoterOverlapmm10_50bpsummits.bed

bedtools intersect -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E10.5_enhancer_peaks.narrowPeak -wo > enhancers/E10.5_enhancerOverlapmm10_50bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E11.5_enhancer_peaks.narrowPeak -wo > enhancers/E11.5_enhancerOverlapmm10_50bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E12.5_enhancer_peaks.narrowPeak -wo > enhancers/E12.5_enhancerOverlapmm10_50bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E13.5_enhancer_peaks.narrowPeak -wo > enhancers/E13.5_enhancerOverlapmm10_50bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E14.5_enhancer_peaks.narrowPeak -wo > enhancers/E14.5_enhancerOverlapmm10_50bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E15.5_enhancer_peaks.narrowPeak -wo > enhancers/E15.5_enhancerOverlapmm10_50bpsummits.bed

bedtools intersect -a promoters/promoter_summit100bp_smiCraTOmm10.bed -b promoters/E10.5.clustered.narrowPeak -wo > promoters/E10.5_promoterOverlapmm10_100bpsummits.bed
bedtools intersect -a promoters/promoter_summit100bp_smiCraTOmm10.bed -b promoters/E11.5.clustered.narrowPeak -wo > promoters/E11.5_promoterOverlapmm10_100bpsummits.bed
bedtools intersect -a promoters/promoter_summit100bp_smiCraTOmm10.bed -b promoters/E12.5.clustered.narrowPeak -wo > promoters/E12.5_promoterOverlapmm10_100bpsummits.bed
bedtools intersect -a promoters/promoter_summit100bp_smiCraTOmm10.bed -b promoters/E13.5.clustered.narrowPeak -wo > promoters/E13.5_promoterOverlapmm10_100bpsummits.bed
bedtools intersect -a promoters/promoter_summit100bp_smiCraTOmm10.bed -b promoters/E14.5.clustered.narrowPeak -wo > promoters/E14.5_promoterOverlapmm10_100bpsummits.bed
bedtools intersect -a promoters/promoter_summit100bp_smiCraTOmm10.bed -b promoters/E15.5.clustered.narrowPeak -wo > promoters/E15.5_promoterOverlapmm10_100bpsummits.bed

bedtools intersect -a enhancers/enhancer_summit100bp_smiCraTOmm10.bed -b enhancers/E10.5_enhancer_peaks.narrowPeak -wo > enhancers/E10.5_enhancerOverlapmm10_100bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit100bp_smiCraTOmm10.bed -b enhancers/E11.5_enhancer_peaks.narrowPeak -wo > enhancers/E11.5_enhancerOverlapmm10_100bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit100bp_smiCraTOmm10.bed -b enhancers/E12.5_enhancer_peaks.narrowPeak -wo > enhancers/E12.5_enhancerOverlapmm10_100bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit100bp_smiCraTOmm10.bed -b enhancers/E13.5_enhancer_peaks.narrowPeak -wo > enhancers/E13.5_enhancerOverlapmm10_100bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit100bp_smiCraTOmm10.bed -b enhancers/E14.5_enhancer_peaks.narrowPeak -wo > enhancers/E14.5_enhancerOverlapmm10_100bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit100bp_smiCraTOmm10.bed -b enhancers/E15.5_enhancer_peaks.narrowPeak -wo > enhancers/E15.5_enhancerOverlapmm10_100bpsummits.bed

bedtools intersect -a promoters/promoter_summit200bp_smiCraTOmm10.bed -b promoters/E10.5.clustered.narrowPeak -wo > promoters/E10.5_promoterOverlapmm10_200bpsummits.bed
bedtools intersect -a promoters/promoter_summit200bp_smiCraTOmm10.bed -b promoters/E11.5.clustered.narrowPeak -wo > promoters/E11.5_promoterOverlapmm10_200bpsummits.bed
bedtools intersect -a promoters/promoter_summit200bp_smiCraTOmm10.bed -b promoters/E12.5.clustered.narrowPeak -wo > promoters/E12.5_promoterOverlapmm10_200bpsummits.bed
bedtools intersect -a promoters/promoter_summit200bp_smiCraTOmm10.bed -b promoters/E13.5.clustered.narrowPeak -wo > promoters/E13.5_promoterOverlapmm10_200bpsummits.bed
bedtools intersect -a promoters/promoter_summit200bp_smiCraTOmm10.bed -b promoters/E14.5.clustered.narrowPeak -wo > promoters/E14.5_promoterOverlapmm10_200bpsummits.bed
bedtools intersect -a promoters/promoter_summit200bp_smiCraTOmm10.bed -b promoters/E15.5.clustered.narrowPeak -wo > promoters/E15.5_promoterOverlapmm10_200bpsummits.bed

bedtools intersect -a enhancers/enhancer_summit200bp_smiCraTOmm10.bed -b enhancers/E10.5_enhancer_peaks.narrowPeak -wo > enhancers/E10.5_enhancerOverlapmm10_200bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit200bp_smiCraTOmm10.bed -b enhancers/E11.5_enhancer_peaks.narrowPeak -wo > enhancers/E11.5_enhancerOverlapmm10_200bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit200bp_smiCraTOmm10.bed -b enhancers/E12.5_enhancer_peaks.narrowPeak -wo > enhancers/E12.5_enhancerOverlapmm10_200bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit200bp_smiCraTOmm10.bed -b enhancers/E13.5_enhancer_peaks.narrowPeak -wo > enhancers/E13.5_enhancerOverlapmm10_200bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit200bp_smiCraTOmm10.bed -b enhancers/E14.5_enhancer_peaks.narrowPeak -wo > enhancers/E14.5_enhancerOverlapmm10_200bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit200bp_smiCraTOmm10.bed -b enhancers/E15.5_enhancer_peaks.narrowPeak -wo > enhancers/E15.5_enhancerOverlapmm10_200bpsummits.bed

bedtools intersect -a promoters/promoter_summit300bp_smiCraTOmm10.bed -b promoters/E10.5.clustered.narrowPeak -wo > promoters/E10.5_promoterOverlapmm10_300bpsummits.bed
bedtools intersect -a promoters/promoter_summit300bp_smiCraTOmm10.bed -b promoters/E11.5.clustered.narrowPeak -wo > promoters/E11.5_promoterOverlapmm10_300bpsummits.bed
bedtools intersect -a promoters/promoter_summit300bp_smiCraTOmm10.bed -b promoters/E12.5.clustered.narrowPeak -wo > promoters/E12.5_promoterOverlapmm10_300bpsummits.bed
bedtools intersect -a promoters/promoter_summit300bp_smiCraTOmm10.bed -b promoters/E13.5.clustered.narrowPeak -wo > promoters/E13.5_promoterOverlapmm10_300bpsummits.bed
bedtools intersect -a promoters/promoter_summit300bp_smiCraTOmm10.bed -b promoters/E14.5.clustered.narrowPeak -wo > promoters/E14.5_promoterOverlapmm10_300bpsummits.bed
bedtools intersect -a promoters/promoter_summit300bp_smiCraTOmm10.bed -b promoters/E15.5.clustered.narrowPeak -wo > promoters/E15.5_promoterOverlapmm10_300bpsummits.bed

bedtools intersect -a enhancers/enhancer_summit300bp_smiCraTOmm10.bed -b enhancers/E10.5_enhancer_peaks.narrowPeak -wo > enhancers/E10.5_enhancerOverlapmm10_300bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit300bp_smiCraTOmm10.bed -b enhancers/E11.5_enhancer_peaks.narrowPeak -wo > enhancers/E11.5_enhancerOverlapmm10_300bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit300bp_smiCraTOmm10.bed -b enhancers/E12.5_enhancer_peaks.narrowPeak -wo > enhancers/E12.5_enhancerOverlapmm10_300bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit300bp_smiCraTOmm10.bed -b enhancers/E13.5_enhancer_peaks.narrowPeak -wo > enhancers/E13.5_enhancerOverlapmm10_300bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit300bp_smiCraTOmm10.bed -b enhancers/E14.5_enhancer_peaks.narrowPeak -wo > enhancers/E14.5_enhancerOverlapmm10_300bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit300bp_smiCraTOmm10.bed -b enhancers/E15.5_enhancer_peaks.narrowPeak -wo > enhancers/E15.5_enhancerOverlapmm10_300bpsummits.bed

bedtools intersect -a promoters/promoter_summit400bp_smiCraTOmm10.bed -b promoters/E10.5.clustered.narrowPeak -wo > promoters/E10.5_promoterOverlapmm10_400bpsummits.bed
bedtools intersect -a promoters/promoter_summit400bp_smiCraTOmm10.bed -b promoters/E11.5.clustered.narrowPeak -wo > promoters/E11.5_promoterOverlapmm10_400bpsummits.bed
bedtools intersect -a promoters/promoter_summit400bp_smiCraTOmm10.bed -b promoters/E12.5.clustered.narrowPeak -wo > promoters/E12.5_promoterOverlapmm10_400bpsummits.bed
bedtools intersect -a promoters/promoter_summit400bp_smiCraTOmm10.bed -b promoters/E13.5.clustered.narrowPeak -wo > promoters/E13.5_promoterOverlapmm10_400bpsummits.bed
bedtools intersect -a promoters/promoter_summit400bp_smiCraTOmm10.bed -b promoters/E14.5.clustered.narrowPeak -wo > promoters/E14.5_promoterOverlapmm10_400bpsummits.bed
bedtools intersect -a promoters/promoter_summit400bp_smiCraTOmm10.bed -b promoters/E15.5.clustered.narrowPeak -wo > promoters/E15.5_promoterOverlapmm10_400bpsummits.bed

bedtools intersect -a enhancers/enhancer_summit400bp_smiCraTOmm10.bed -b enhancers/E10.5_enhancer_peaks.narrowPeak -wo > enhancers/E10.5_enhancerOverlapmm10_400bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit400bp_smiCraTOmm10.bed -b enhancers/E11.5_enhancer_peaks.narrowPeak -wo > enhancers/E11.5_enhancerOverlapmm10_400bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit400bp_smiCraTOmm10.bed -b enhancers/E12.5_enhancer_peaks.narrowPeak -wo > enhancers/E12.5_enhancerOverlapmm10_400bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit400bp_smiCraTOmm10.bed -b enhancers/E13.5_enhancer_peaks.narrowPeak -wo > enhancers/E13.5_enhancerOverlapmm10_400bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit400bp_smiCraTOmm10.bed -b enhancers/E14.5_enhancer_peaks.narrowPeak -wo > enhancers/E14.5_enhancerOverlapmm10_400bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit400bp_smiCraTOmm10.bed -b enhancers/E15.5_enhancer_peaks.narrowPeak -wo > enhancers/E15.5_enhancerOverlapmm10_400bpsummits.bed

bedtools intersect -a promoters/promoter_summit500bp_smiCraTOmm10.bed -b promoters/E10.5.clustered.narrowPeak -wo > promoters/E10.5_promoterOverlapmm10_500bpsummits.bed
bedtools intersect -a promoters/promoter_summit500bp_smiCraTOmm10.bed -b promoters/E11.5.clustered.narrowPeak -wo > promoters/E11.5_promoterOverlapmm10_500bpsummits.bed
bedtools intersect -a promoters/promoter_summit500bp_smiCraTOmm10.bed -b promoters/E12.5.clustered.narrowPeak -wo > promoters/E12.5_promoterOverlapmm10_500bpsummits.bed
bedtools intersect -a promoters/promoter_summit500bp_smiCraTOmm10.bed -b promoters/E13.5.clustered.narrowPeak -wo > promoters/E13.5_promoterOverlapmm10_500bpsummits.bed
bedtools intersect -a promoters/promoter_summit500bp_smiCraTOmm10.bed -b promoters/E14.5.clustered.narrowPeak -wo > promoters/E14.5_promoterOverlapmm10_500bpsummits.bed
bedtools intersect -a promoters/promoter_summit500bp_smiCraTOmm10.bed -b promoters/E15.5.clustered.narrowPeak -wo > promoters/E15.5_promoterOverlapmm10_500bpsummits.bed

bedtools intersect -a enhancers/enhancer_summit500bp_smiCraTOmm10.bed -b enhancers/E10.5_enhancer_peaks.narrowPeak -wo > enhancers/E10.5_enhancerOverlapmm10_500bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit500bp_smiCraTOmm10.bed -b enhancers/E11.5_enhancer_peaks.narrowPeak -wo > enhancers/E11.5_enhancerOverlapmm10_500bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit500bp_smiCraTOmm10.bed -b enhancers/E12.5_enhancer_peaks.narrowPeak -wo > enhancers/E12.5_enhancerOverlapmm10_500bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit500bp_smiCraTOmm10.bed -b enhancers/E13.5_enhancer_peaks.narrowPeak -wo > enhancers/E13.5_enhancerOverlapmm10_500bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit500bp_smiCraTOmm10.bed -b enhancers/E14.5_enhancer_peaks.narrowPeak -wo > enhancers/E14.5_enhancerOverlapmm10_500bpsummits.bed
bedtools intersect -a enhancers/enhancer_summit500bp_smiCraTOmm10.bed -b enhancers/E15.5_enhancer_peaks.narrowPeak -wo > enhancers/E15.5_enhancerOverlapmm10_500bpsummits.bed

## Window allows intersections within 50 bps. 
bedtools window -w 50 -u -a promoters/clustered/promoter_summit50bp_smiCraTOmm10.bed -b promoters/clustered/E10.5.clustered.narrowPeak  > promoters/clustered/E10.5_promoterOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a promoters/clustered/promoter_summit50bp_smiCraTOmm10.bed -b promoters/clustered/E11.5.clustered.narrowPeak  > promoters/clustered/E11.5_promoterOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a promoters/clustered/promoter_summit50bp_smiCraTOmm10.bed -b promoters/clustered/E12.5.clustered.narrowPeak  > promoters/clustered/E12.5_promoterOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a promoters/clustered/promoter_summit50bp_smiCraTOmm10.bed -b promoters/clustered/E13.5.clustered.narrowPeak  > promoters/clustered/E13.5_promoterOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a promoters/clustered/promoter_summit50bp_smiCraTOmm10.bed -b promoters/clustered/E14.5.clustered.narrowPeak  > promoters/clustered/E14.5_promoterOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a promoters/clustered/promoter_summit50bp_smiCraTOmm10.bed -b promoters/clustered/E15.5.clustered.narrowPeak  > promoters/clustered/E15.5_promoterOverlapmm10_50bpsummits_window.bed

bedtools window -w 50 -u -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E10.5_enhancer_peaks.narrowPeak  > enhancers/E10.5_enhancerOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E11.5_enhancer_peaks.narrowPeak  > enhancers/E11.5_enhancerOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E12.5_enhancer_peaks.narrowPeak  > enhancers/E12.5_enhancerOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E13.5_enhancer_peaks.narrowPeak  > enhancers/E13.5_enhancerOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E14.5_enhancer_peaks.narrowPeak  > enhancers/E14.5_enhancerOverlapmm10_50bpsummits_window.bed
bedtools window -w 50 -u -a enhancers/enhancer_summit50bp_smiCraTOmm10.bed -b enhancers/E15.5_enhancer_peaks.narrowPeak  > enhancers/E15.5_enhancerOverlapmm10_50bpsummits_window.bed

## Lift dunnart peaks back over to the dunnart genome
liftOver -bedPlus=4 promoters/clustered/E10.5_promoterOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/clustered/E10.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed promoters/clustered/E10.5_promoterOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoters/clustered/E11.5_promoterOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/clustered/E11.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed promoters/clustered/E11.5_promoterOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoters/clustered/E12.5_promoterOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/clustered/E12.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed promoters/clustered/E12.5_promoterOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoters/clustered/E13.5_promoterOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/clustered/E13.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed promoters/clustered/E13.5_promoterOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoters/clustered/E14.5_promoterOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/clustered/E14.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed promoters/clustered/E14.5_promoterOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoters/clustered/E15.5_promoterOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoters/clustered/E15.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed promoters/clustered/E15.5_promoterOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed

liftOver -bedPlus=4 enhancers/E10.5_enhancerOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/E10.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed enhancers/E10.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancers/E11.5_enhancerOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/E11.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed enhancers/E11.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancers/E12.5_enhancerOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/E12.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed enhancers/E12.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancers/E13.5_enhancerOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/E13.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed enhancers/E13.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancers/E14.5_enhancerOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/E14.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed enhancers/E14.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancers/E15.5_enhancerOverlapmm10_50bpsummits_window.bed ../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancers/E15.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed enhancers/E15.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra_unmapped.bed

awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E10.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E11.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E12.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E13.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E15.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E14.5_enhancerOverlapmm10_50bpsummits_backTOsmiCra.bed

awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E10.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E11.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E12.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E13.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E15.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed
awk 'x[$4]++ == 1 { print $4 " is duplicated"}' E14.5_promoterOverlapmm10_50bpsummits_backTOsmiCra.bed


### When you lift over a peak from one species to another, does it always land next to the same gene in both species?
## Use annotated peak files for all peaks and compare nearest gene call
## Intersect annotation file with peak summits

## Promoters
bedtools intersect -a promoter_overlap_50bpsummits.bed -b cluster1_promoters_annotation.bed -wb > dunnart_promoter_50bpsummits_annotation.bed
bedtools intersect -a E10_promoter_overlap_50bpsummits.bed -b clustered.E10.filtered_annotation.bed -wo > E10_promoter_50bpsummits_annotation.bed
bedtools intersect -a E11_promoter_overlap_50bpsummits.bed -b clustered.E11.filtered_annotation.bed -wo > E11_promoter_50bpsummits_annotation.bed
bedtools intersect -a E12_promoter_overlap_50bpsummits.bed -b clustered.E12.filtered_annotation.bed -wo > E12_promoter_50bpsummits_annotation.bed
bedtools intersect -a E13_promoter_overlap_50bpsummits.bed -b clustered.E13.filtered_annotation.bed -wo > E13_promoter_50bpsummits_annotation.bed
bedtools intersect -a E14_promoter_overlap_50bpsummits.bed -b clustered.E14.filtered_annotation.bed -wo > E14_promoter_50bpsummits_annotation.bed
bedtools intersect -a E15_promoter_overlap_50bpsummits.bed -b clustered.E15.filtered_annotation.bed -wo > E15_promoter_50bpsummits_annotation.bed

bedtools intersect -a promoter_overlap_100bpsummits.bed -b cluster1_promoters_annotation.bed -wb > dunnart_promoter_100bpsummits_annotation.bed
bedtools intersect -a E10_promoter_overlap_100bpsummits.bed -b clustered.E10.filtered_annotation.bed -wo > E10_promoter_100bpsummits_annotation.bed
bedtools intersect -a E11_promoter_overlap_100bpsummits.bed -b clustered.E11.filtered_annotation.bed -wo > E11_promoter_100bpsummits_annotation.bed
bedtools intersect -a E12_promoter_overlap_100bpsummits.bed -b clustered.E12.filtered_annotation.bed -wo > E12_promoter_100bpsummits_annotation.bed
bedtools intersect -a E13_promoter_overlap_100bpsummits.bed -b clustered.E13.filtered_annotation.bed -wo > E13_promoter_100bpsummits_annotation.bed
bedtools intersect -a E14_promoter_overlap_100bpsummits.bed -b clustered.E14.filtered_annotation.bed -wo > E14_promoter_100bpsummits_annotation.bed
bedtools intersect -a E15_promoter_overlap_100bpsummits.bed -b clustered.E15.filtered_annotation.bed -wo > E15_promoter_100bpsummits_annotation.bed

bedtools intersect -a promoter_overlap_200bpsummits.bed -b cluster1_promoters_annotation.bed -wb > dunnart_promoter_200bpsummits_annotation.bed
bedtools intersect -a E10_promoter_overlap_200bpsummits.bed -b clustered.E10.filtered_annotation.bed -wo > E10_promoter_200bpsummits_annotation.bed
bedtools intersect -a E11_promoter_overlap_200bpsummits.bed -b clustered.E11.filtered_annotation.bed -wo > E11_promoter_200bpsummits_annotation.bed
bedtools intersect -a E12_promoter_overlap_200bpsummits.bed -b clustered.E12.filtered_annotation.bed -wo > E12_promoter_200bpsummits_annotation.bed
bedtools intersect -a E13_promoter_overlap_200bpsummits.bed -b clustered.E13.filtered_annotation.bed -wo > E13_promoter_200bpsummits_annotation.bed
bedtools intersect -a E14_promoter_overlap_200bpsummits.bed -b clustered.E14.filtered_annotation.bed -wo > E14_promoter_200bpsummits_annotation.bed
bedtools intersect -a E15_promoter_overlap_200bpsummits.bed -b clustered.E15.filtered_annotation.bed -wo > E15_promoter_200bpsummits_annotation.bed

bedtools intersect -a promoter_overlap_300bpsummits.bed -b cluster1_promoters_annotation.bed -wb > dunnart_promoter_300bpsummits_annotation.bed
bedtools intersect -a E10_promoter_overlap_300bpsummits.bed -b clustered.E10.filtered_annotation.bed -wo > E10_promoter_300bpsummits_annotation.bed
bedtools intersect -a E11_promoter_overlap_300bpsummits.bed -b clustered.E11.filtered_annotation.bed -wo > E11_promoter_300bpsummits_annotation.bed
bedtools intersect -a E12_promoter_overlap_300bpsummits.bed -b clustered.E12.filtered_annotation.bed -wo > E12_promoter_300bpsummits_annotation.bed
bedtools intersect -a E13_promoter_overlap_300bpsummits.bed -b clustered.E13.filtered_annotation.bed -wo > E13_promoter_300bpsummits_annotation.bed
bedtools intersect -a E14_promoter_overlap_300bpsummits.bed -b clustered.E14.filtered_annotation.bed -wo > E14_promoter_300bpsummits_annotation.bed
bedtools intersect -a E15_promoter_overlap_300bpsummits.bed -b clustered.E15.filtered_annotation.bed -wo > E15_promoter_300bpsummits_annotation.bed

bedtools intersect -a promoter_overlap_400bpsummits.bed -b cluster1_promoters_annotation.bed -wb > dunnart_promoter_400bpsummits_annotation.bed
bedtools intersect -a E10_promoter_overlap_400bpsummits.bed -b clustered.E10.filtered_annotation.bed -wo > E10_promoter_400bpsummits_annotation.bed
bedtools intersect -a E11_promoter_overlap_400bpsummits.bed -b clustered.E11.filtered_annotation.bed -wo > E11_promoter_400bpsummits_annotation.bed
bedtools intersect -a E12_promoter_overlap_400bpsummits.bed -b clustered.E12.filtered_annotation.bed -wo > E12_promoter_400bpsummits_annotation.bed
bedtools intersect -a E13_promoter_overlap_400bpsummits.bed -b clustered.E13.filtered_annotation.bed -wo > E13_promoter_400bpsummits_annotation.bed
bedtools intersect -a E14_promoter_overlap_400bpsummits.bed -b clustered.E14.filtered_annotation.bed -wo > E14_promoter_400bpsummits_annotation.bed
bedtools intersect -a E15_promoter_overlap_400bpsummits.bed -b clustered.E15.filtered_annotation.bed -wo > E15_promoter_400bpsummits_annotation.bed


bedtools intersect -a promoter_overlap_500bpsummits.bed -b cluster1_promoters_annotation.bed -wb > dunnart_promoter_500bpsummits_annotation.bed
bedtools intersect -a E10_promoter_overlap_500bpsummits.bed -b clustered.E10.filtered_annotation.bed -wo > E10_promoter_500bpsummits_annotation.bed
bedtools intersect -a E11_promoter_overlap_500bpsummits.bed -b clustered.E11.filtered_annotation.bed -wo > E11_promoter_500bpsummits_annotation.bed
bedtools intersect -a E12_promoter_overlap_500bpsummits.bed -b clustered.E12.filtered_annotation.bed -wo > E12_promoter_500bpsummits_annotation.bed
bedtools intersect -a E13_promoter_overlap_500bpsummits.bed -b clustered.E13.filtered_annotation.bed -wo > E13_promoter_500bpsummits_annotation.bed
bedtools intersect -a E14_promoter_overlap_500bpsummits.bed -b clustered.E14.filtered_annotation.bed -wo > E14_promoter_500bpsummits_annotation.bed
bedtools intersect -a E15_promoter_overlap_500bpsummits.bed -b clustered.E15.filtered_annotation.bed -wo > E15_promoter_500bpsummits_annotation.bed


liftOver -bedPlus=4 dunnart_promoter_50bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_50bpsummits_annotation_smiCraTOmm10.bed promoter_overlap_50bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_promoter_100bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_100bpsummits_annotation_smiCraTOmm10.bed promoter_overlap_100bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_promoter_200bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_200bpsummits_annotation_smiCraTOmm10.bed promoter_overlap_200bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_promoter_300bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_300bpsummits_annotation_smiCraTOmm10.bed promoter_overlap_300bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_promoter_400bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_400bpsummits_annotation_smiCraTOmm10.bed promoter_overlap_400bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_promoter_500bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_500bpsummits_annotation_smiCraTOmm10.bed promoter_overlap_500bpsummits_annotation_smiCraTOmm10_unmapped.bed

liftOver -bedPlus=4 promoter_overlap_50bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_50bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed promoter_overlap_50bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoter_overlap_100bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_100bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed promoter_overlap_100bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoter_overlap_200bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_200bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed promoter_overlap_200bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoter_overlap_300bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_300bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed promoter_overlap_300bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoter_overlap_400bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_400bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed promoter_overlap_400bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 promoter_overlap_500bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain promoter_overlap_500bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed promoter_overlap_500bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed

## Enhancers
bedtools intersect -a enhancer_overlap_50bpsummits.bed -b dunnart_enhancer_annotation.bed -wb > dunnart_enhancer_50bpsummits_annotation.bed
bedtools intersect -a E10_enhancer_overlap_50bpsummits.bed -b E10.5_enhancer_annotation.bed -wo > E10_enhancer_50bpsummits_annotation.bed
bedtools intersect -a E11_enhancer_overlap_50bpsummits.bed -b E11.5_enhancer_annotation.bed -wo > E11_enhancer_50bpsummits_annotation.bed
bedtools intersect -a E12_enhancer_overlap_50bpsummits.bed -b E12.5_enhancer_annotation.bed -wo > E12_enhancer_50bpsummits_annotation.bed
bedtools intersect -a E13_enhancer_overlap_50bpsummits.bed -b E13.5_enhancer_annotation.bed -wo > E13_enhancer_50bpsummits_annotation.bed
bedtools intersect -a E14_enhancer_overlap_50bpsummits.bed -b E14.5_enhancer_annotation.bed -wo > E14_enhancer_50bpsummits_annotation.bed
bedtools intersect -a E15_enhancer_overlap_50bpsummits.bed -b E15.5_enhancer_annotation.bed -wo > E15_enhancer_50bpsummits_annotation.bed

bedtools intersect -a enhancer_overlap_100bpsummits.bed -b dunnart_enhancer_annotation.bed -wb > dunnart_enhancer_100bpsummits_annotation.bed
bedtools intersect -a E10_enhancer_overlap_100bpsummits.bed -b E10.5_enhancer_annotation.bed -wo > E10_enhancer_100bpsummits_annotation.bed
bedtools intersect -a E11_enhancer_overlap_100bpsummits.bed -b E11.5_enhancer_annotation.bed -wo > E11_enhancer_100bpsummits_annotation.bed
bedtools intersect -a E12_enhancer_overlap_100bpsummits.bed -b E12.5_enhancer_annotation.bed -wo > E12_enhancer_100bpsummits_annotation.bed
bedtools intersect -a E13_enhancer_overlap_100bpsummits.bed -b E13.5_enhancer_annotation.bed -wo > E13_enhancer_100bpsummits_annotation.bed
bedtools intersect -a E14_enhancer_overlap_100bpsummits.bed -b E14.5_enhancer_annotation.bed -wo > E14_enhancer_100bpsummits_annotation.bed
bedtools intersect -a E15_enhancer_overlap_100bpsummits.bed -b E15.5_enhancer_annotation.bed -wo > E15_enhancer_100bpsummits_annotation.bed

bedtools intersect -a enhancer_overlap_200bpsummits.bed -b dunnart_enhancer_annotation.bed -wb > dunnart_enhancer_200bpsummits_annotation.bed
bedtools intersect -a E10_enhancer_overlap_200bpsummits.bed -b E10.5_enhancer_annotation.bed -wo > E10_enhancer_200bpsummits_annotation.bed
bedtools intersect -a E11_enhancer_overlap_200bpsummits.bed -b E11.5_enhancer_annotation.bed -wo > E11_enhancer_200bpsummits_annotation.bed
bedtools intersect -a E12_enhancer_overlap_200bpsummits.bed -b E12.5_enhancer_annotation.bed -wo > E12_enhancer_200bpsummits_annotation.bed
bedtools intersect -a E13_enhancer_overlap_200bpsummits.bed -b E13.5_enhancer_annotation.bed -wo > E13_enhancer_200bpsummits_annotation.bed
bedtools intersect -a E14_enhancer_overlap_200bpsummits.bed -b E14.5_enhancer_annotation.bed -wo > E14_enhancer_200bpsummits_annotation.bed
bedtools intersect -a E15_enhancer_overlap_200bpsummits.bed -b E15.5_enhancer_annotation.bed -wo > E15_enhancer_200bpsummits_annotation.bed

bedtools intersect -a enhancer_overlap_300bpsummits.bed -b dunnart_enhancer_annotation.bed -wb > dunnart_enhancer_300bpsummits_annotation.bed
bedtools intersect -a E10_enhancer_overlap_300bpsummits.bed -b E10.5_enhancer_annotation.bed -wo > E10_enhancer_300bpsummits_annotation.bed
bedtools intersect -a E11_enhancer_overlap_300bpsummits.bed -b E11.5_enhancer_annotation.bed -wo > E11_enhancer_300bpsummits_annotation.bed
bedtools intersect -a E12_enhancer_overlap_300bpsummits.bed -b E12.5_enhancer_annotation.bed -wo > E12_enhancer_300bpsummits_annotation.bed
bedtools intersect -a E13_enhancer_overlap_300bpsummits.bed -b E13.5_enhancer_annotation.bed -wo > E13_enhancer_300bpsummits_annotation.bed
bedtools intersect -a E14_enhancer_overlap_300bpsummits.bed -b E14.5_enhancer_annotation.bed -wo > E14_enhancer_300bpsummits_annotation.bed
bedtools intersect -a E15_enhancer_overlap_300bpsummits.bed -b E15.5_enhancer_annotation.bed -wo > E15_enhancer_300bpsummits_annotation.bed

bedtools intersect -a enhancer_overlap_400bpsummits.bed -b dunnart_enhancer_annotation.bed -wb > dunnart_enhancer_400bpsummits_annotation.bed
bedtools intersect -a E10_enhancer_overlap_400bpsummits.bed -b E10.5_enhancer_annotation.bed -wo > E10_enhancer_400bpsummits_annotation.bed
bedtools intersect -a E11_enhancer_overlap_400bpsummits.bed -b E11.5_enhancer_annotation.bed -wo > E11_enhancer_400bpsummits_annotation.bed
bedtools intersect -a E12_enhancer_overlap_400bpsummits.bed -b E12.5_enhancer_annotation.bed -wo > E12_enhancer_400bpsummits_annotation.bed
bedtools intersect -a E13_enhancer_overlap_400bpsummits.bed -b E13.5_enhancer_annotation.bed -wo > E13_enhancer_400bpsummits_annotation.bed
bedtools intersect -a E14_enhancer_overlap_400bpsummits.bed -b E14.5_enhancer_annotation.bed -wo > E14_enhancer_400bpsummits_annotation.bed
bedtools intersect -a E15_enhancer_overlap_400bpsummits.bed -b E15.5_enhancer_annotation.bed -wo > E15_enhancer_400bpsummits_annotation.bed


bedtools intersect -a enhancer_overlap_500bpsummits.bed -b dunnart_enhancer_annotation.bed -wb > dunnart_enhancer_500bpsummits_annotation.bed
bedtools intersect -a E10_enhancer_overlap_500bpsummits.bed -b E10.5_enhancer_annotation.bed -wo > E10_enhancer_500bpsummits_annotation.bed
bedtools intersect -a E11_enhancer_overlap_500bpsummits.bed -b E11.5_enhancer_annotation.bed -wo > E11_enhancer_500bpsummits_annotation.bed
bedtools intersect -a E12_enhancer_overlap_500bpsummits.bed -b E12.5_enhancer_annotation.bed -wo > E12_enhancer_500bpsummits_annotation.bed
bedtools intersect -a E13_enhancer_overlap_500bpsummits.bed -b E13.5_enhancer_annotation.bed -wo > E13_enhancer_500bpsummits_annotation.bed
bedtools intersect -a E14_enhancer_overlap_500bpsummits.bed -b E14.5_enhancer_annotation.bed -wo > E14_enhancer_500bpsummits_annotation.bed
bedtools intersect -a E15_enhancer_overlap_500bpsummits.bed -b E15.5_enhancer_annotation.bed -wo > E15_enhancer_500bpsummits_annotation.bed

liftOver -bedPlus=4 dunnart_enhancer_50bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_50bpsummits_annotation_smiCraTOmm10.bed enhancer_overlap_50bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_enhancer_100bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_100bpsummits_annotation_smiCraTOmm10.bed enhancer_overlap_100bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_enhancer_200bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_200bpsummits_annotation_smiCraTOmm10.bed enhancer_overlap_200bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_enhancer_300bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_300bpsummits_annotation_smiCraTOmm10.bed enhancer_overlap_300bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_enhancer_400bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_400bpsummits_annotation_smiCraTOmm10.bed enhancer_overlap_400bpsummits_annotation_smiCraTOmm10_unmapped.bed
liftOver -bedPlus=4 dunnart_enhancer_500bpsummits_annotation.bed ../../smiCra1TOmm10_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_500bpsummits_annotation_smiCraTOmm10.bed enhancer_overlap_500bpsummits_annotation_smiCraTOmm10_unmapped.bed

liftOver -bedPlus=4 enhancer_overlap_50bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_50bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed enhancer_overlap_50bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancer_overlap_100bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_100bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed enhancer_overlap_100bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancer_overlap_200bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_200bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed enhancer_overlap_200bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancer_overlap_300bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_300bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed enhancer_overlap_300bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancer_overlap_400bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_400bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed enhancer_overlap_400bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed
liftOver -bedPlus=4 enhancer_overlap_500bpsummits_annotation_smiCraTOmm10.bed ../../mm10TOsmiCra1_patched_sorted_repFil_chainCl_preNet_chainNet_stitched.chain enhancer_overlap_500bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra.bed enhancer_overlap_500bpsummits_annotation_smiCraTOmm10_mm10TOsmiCra_unmapped.bed

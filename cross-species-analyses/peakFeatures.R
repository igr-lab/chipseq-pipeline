## This script explores various features of the dunnart peaks including
## Peak intensity, lengths, distance to TSS, filtering peaks.

# Load in libraries
library(data.table) 
library(tidyverse)
library(ggridges)
library(ggpubr)
library(reshape2)
library(RColorBrewer)
library(ggplot2)
library(VennDiagram)
library(viridis)
library(hrbrthemes)
library(gghalves)
library(multcompView)

setwd("/data/projects/punim0586/lecook/chipseq-pipeline/cross_species/peakAnnotation")

font = theme(axis.text.x = element_text(size = 25),
        axis.text.y = element_text(size = 25),  
        axis.title.x = element_text(size = 25),
        axis.title.y = element_text(size = 25), 
        legend.title=element_text(size=25), legend.text=element_text(size=25))

plot_dir='/data/projects/punim0586/lecook/chipseq-pipeline/cross_species/peakAnnotation/consensus'

###########################################################################################################
############################################## Peak Features ##############################################
###########################################################################################################

unfilteredPeakFeatures <- function(fileList, combined.tbl.stack, peak.length.plot, peak.intensity.plot){
    files =list.files(fileList, pattern= ".narrowPeak", full.names=T) # create list of files in directory
    files = as.list(files)
    data = lapply(files, function(x) fread(x, header=FALSE, sep="\t", quote = "", na.strings=c("", "NA")))
    names(data) = c("enhancers","promoters","H3K27ac_only","H3K27ac","H3K4me3_H3K27ac", "H3K4me3_only","H3K4me3")
    df1 = Map(mutate, data, group = names(data))
    df1$H3K27ac_only$cre = "enhancers"
    df1$H3K4me3_only$cre = "promoters"
    df1$H3K4me3_H3K27ac$cre = "promoters"

    fullPeaks = rbind(df1$enhancers, df1$promoters, df1$H3K27ac, df1$H3K4me3)
    combinedPeaks = rbind(df1$H3K27ac_only, df1$H3K4me3_H3K27ac, df1$H3K4me3_only)
        
    ## Plot stacked bar graph with number of peaks for each histone mark
    combined.tbl <- with(combinedPeaks, table(group, cre))
    combined.tbl <- as.data.frame(combined.tbl)
    pdf(file=paste0(plot_dir, combined.tbl.stack), width = 10, height = 7)
      ggplot(combined.tbl, aes(factor(cre), Freq, fill=group)) + 
      geom_bar(position=position_stack(reverse = TRUE), stat="identity") +
      scale_fill_manual(values = c("#FF9D40",  "#37B6CE", "#04859D"))  + 
      theme_minimal() + ylab("Number of peaks") +
      xlab("")
    dev.off()

    # Peak lengths for H3K4me3 and H3K27ac
    fullPeaks$length <- (fullPeaks$V3 - fullPeaks$V2)
    level_order <- c('promoter', 'enhancer', 'H3K4me3', 'H3K27ac')

    ## Plot peak lengths
    p <- ggplot(fullPeaks, aes(factor(group, level = level_order), y = log10(length))) + 
    geom_half_violin(aes(fill=factor(group), color=factor(group)),
    side = "r", 
    position = "dodge"
    )+
    geom_boxplot(aes(color=factor(group)),
    width = .15, 
    outlier.shape = NA,
    fill=c("#FCF8EC","#FCF8EC","#F5F6F7","#E4F3F1"),
    position = position_dodge(width=.1)
    ) +
    coord_cartesian(xlim = c(1.2, NA), clip = "off") + theme_bw() + xlab("") + ylab("Log10 Peak Length") 
    pdf(file=paste0(plot_dir, peak.length.plot), width = 10, height = 7)
    print(p +  scale_color_manual(values = c("#E9C46A","#E9C46A","#264653","#2A9D8F")) +
    scale_fill_manual(values = c("#F1DAA2","#F1DAA2","#AEBABF","#7AC2B9")) + coord_flip())
    dev.off()

    ## Plot peak intensity
    p <- ggplot(fullPeaks, aes(factor(group, level = level_order), y = log10(V7))) + 
    geom_half_violin(aes(fill=factor(group), color=factor(group)),
    side = "r", 
    position = "dodge"
    )+
    geom_boxplot(aes(color=factor(group)),
    width = .15, 
    outlier.shape = NA,
    fill=c("#FCF8EC","#FCF8EC","#F5F6F7","#E4F3F1"),
    position = position_dodge(width=.1)
    ) +
    coord_cartesian(xlim = c(1.2, NA), clip = "off") + theme_bw() + xlab("") + ylab("Log10 Peak Intensity") 
    pdf(file=paste0(plot_dir, peak.intensity.plot), width = 10, height = 7)
    print(p +  scale_color_manual(values = c("#E9C46A","#E9C46A","#264653","#2A9D8F")) +
    scale_fill_manual(values = c("#F1DAA2","#F1DAA2","#AEBABF","#7AC2B9")) + coord_flip())
    dev.off()  
}

unfilteredPeakFeatures(fileList = fullPeak_dir, combined.tbl.stack= "dunnart_combined_stacked.pdf",
peak.length.plot = "dunnart_fullPeaks_length.pdf", peak.intensity.plot = "dunnart_fullPeaks_intensity.pdf")

## Now see where the peaks are located in relation to the TSS
## Promoters should be reasonably close to the TSS and enhancers more distal to the TSS
## Plot distance to TSS for all unfiltered peaks 

distanceToTSS <- function(fileList, enhancerTSSplot, promoterTSSplot, TSSplot){
    files =list.files(fileList, pattern= "annotation.txt", full.names=T) # create list of files in directory
    files = as.list(files)
    data = lapply(files, function(x) fread(x, header=TRUE, sep="\t", quote = "", na.strings=c("", "NA"))) # read in all files
    names(data) = c("enhancers","promoters","H3K27ac","H3K4me3")
    df1 = Map(mutate, data, mark = names(data))
    enhProm = list(df1$promoters, df1$enhancers)
    enhProm = suppressWarnings(lapply(enhProm, function(x) x[,log10_abs_dist:=log10(abs(distanceToTSS)+1)][,log10_abs_dist:=ifelse(distanceToTSS<0,-log10_abs_dist,log10_abs_dist)]))
    names(enhProm) = c("promoters","enhancers")

    pdf(file = paste0(plot_dir, enhancerTSSplot), width=10, height = 7)
    print(ggplot(enhProm$enhancers, aes(x=log10_abs_dist)) + 
    geom_density(alpha=0.5, color="#2A9D8F", fill="#2A9D8F") + 
    scale_y_continuous(limits=c(0,0.3), labels = scales::percent) + 
    scale_x_continuous(limits=c(-7, 7)) + theme_bw())
    dev.off()

    pdf(file = paste0(plot_dir, promoterTSSplot), width=10, height = 7)
    print(ggplot(enhProm$promoters, aes(x=log10_abs_dist)) + 
    geom_density(alpha=0.5, color="#2A9D8F", fill="#2A9D8F") + 
    scale_y_continuous(limits=c(0,0.3), labels = scales::percent) + 
    scale_x_continuous(limits=c(-7, 7)) + theme_bw())
    dev.off()

    enhProm = rbindlist(enhProm,)
    pdf(file = paste0(plot_dir, TSSplot), width=10, height = 7)
    print(ggplot(enhProm, aes(x=log10_abs_dist, fill=factor(mark), color=factor(mark))) + 
    geom_density(alpha=0.5) +scale_color_manual(values=c("#E9C46A", "#2A9D8F")) + scale_fill_manual(values=c("#E9C46A", "#2A9D8F")) + 
    scale_x_continuous(limits=c(-7, 7)) + theme_bw() + font)
    dev.off()

}
distanceToTSS(fileList = annot_dir, enhancerTSSplot="dunnart_enhancerTSS.pdf", 
promoterTSSplot="dunnart_promoterTSS.pdf", TSSplot = "dunnart_TSS.pdf")

#########################################################################################################################
################################################### K-MEAN CLUSTERING ###################################################
#########################################################################################################################
### From this we can see that the promoter peaks have a large amount of peaks a long way from the TSS
### Suggests that these are either actually enhancers or they represent unannotated transcripts
### Probably a mixture of both based on comparison with mouse peaks (where the annotation is better) there are not as many
### peaks in this distal regions. 
### Use k-means clustering to group the peaks to decide on a cut off for "promoter" peaks. This will be more conservative for
### what we identify as promoters.

font = theme(axis.text.x = element_text(size = 25),
        axis.text.y = element_text(size = 25),  
        axis.title.x = element_text(size = 25),
        axis.title.y = element_text(size = 25), 
        legend.title=element_text(size=25), legend.text=element_text(size=25))

setwd("/data/projects/punim0586/lecook/chipseq-pipeline/cross_species/peakAnnotation/consensus")

## k-means clustering 

kmeansCluster <- function(file, plot1, plot2, output){
  data = fread(file, header=TRUE, sep="\t", quote = "") # read in all files
  data = data[,log10_dist:=log10(abs(distanceToTSS)+1.1)][,log10_dist:=ifelse(distanceToTSS<0,-log10_dist,log10_dist)]
  data = data[,abs_dist:=log10(abs(distanceToTSS)+1.1)]

  data = data %>% dplyr::select("V4", "width", "V7", "distanceToTSS", "log10_dist", "abs_dist", "annotation")
  
  ## plot the number of peaks in each cluster
  ## Using the MacQueen algorithm instead of the default Lloyd 
  ## The algorithm is more efficient as it updates centroids more often and usually needs to
  ## perform one complete pass through the cases to converge on a solution.
  df = data[,5]
  cre.kmeans = kmeans(df, 3, iter.max=100, nstart=25, algorithm="MacQueen")  
  cre.kmeans_table = data.frame(cre.kmeans$size, cre.kmeans$centers)
  cre.kmeans_df = data.frame(Cluster = cre.kmeans$cluster, data)

  pdf(plot1, width=10, height = 7)
  p <- ggplot(data = cre.kmeans_df, aes(y = Cluster, fill=as.factor(Cluster), color=as.factor(Cluster))) +
  geom_bar()  +
  theme(plot.title = element_text(hjust = 0.5)) + theme_bw() + font
  pdf(plot1)
  print(p + scale_color_manual(values = c('#9EBCDA','#8C6BB1', "#4D004B")) + scale_fill_manual(values = c('#9EBCDA','#8C6BB1', "#4D004B")))
  dev.off()


  p <- ggplot(cre.kmeans_df, aes(x=log10_dist, fill=as.factor(Cluster), color=as.factor(Cluster))) + 
            geom_histogram(binwidth=0.1, position = 'identity')  +
            theme_bw()+ font
  pdf(plot2, width = 10, height = 7)
  print(p + scale_color_manual(values = c('#9EBCDA','#8C6BB1', "#4D004B")) + scale_fill_manual(values = c('#9EBCDA','#8C6BB1', "#4D004B")))
  dev.off()  

  write.table(cre.kmeans_df, output, sep="\t", quote=F, row.names=F)
  return(cre.kmeans_df)
}
promoter_kmeans = kmeansCluster(file = "consensus/promoters/annotation/dunnart_promoter_annotation.txt",  plot1 = "promoter_kmeans_bar.pdf", plot2 = "promoter_kmeans_histogram.pdf", output = "promoter_kmeans_peaks.txt")

## Extract cluster groups from narrowPeak files and save separately
cluster1 <- promoter_kmeans$V4[promoter_kmeans$Cluster==1]
cluster2 <- promoter_kmeans$V4[promoter_kmeans$Cluster==2]
cluster3 <- promoter_kmeans$V4[promoter_kmeans$Cluster==3]

extractClusters = function(promoter, cluster1, cluster2, cluster3, out1, out2, out3){
  file= fread(promoter, header=FALSE, sep="\t", quote = "") 
  write.table(subset(file, V4 %in% cluster1), out1, quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
  write.table(subset(file, V4 %in% cluster2), out2, quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
  write.table(subset(file, V4 %in% cluster3), out3, quote=FALSE, col.names=FALSE, row.names=FALSE, sep="\t")
}

extractClusters(promoter = "consensus/promoters/dunnart_promoter_peaks.narrowPeak", out1 = "cluster1_promoters.narrowPeak", 
out2 ="cluster2_promoters.narrowPeak" , out3 = "cluster3_promoters.narrowPeak", cluster1 = cluster1, 
cluster2 = cluster2, cluster3 = cluster3)

## Filter mouse peaks two the same range as the dunnart clusters
filterMousePeaks = function(fileList, plot_dir){
  files =list.files(fileList, pattern= "5_promoter_peaks.narrowPeak", full.names=T) # create list of files in directory
  files = as.list(files)
  data = lapply(files, function(x) fread(x, header=TRUE, sep="\t", quote = "", na.strings=c("", "NA"))) # read in all files
  names(data) = c("E10","E11","E12","E13","E14", "E15")
  df1 = Map(mutate, data, group = names(data))
  df1 = lapply(df1, function(x) x %>% filter(x$distanceToTSS<200 & x$distanceToTSS>-200))    
  lapply(names(df1), function(x) write.table(df1[[x]], file=paste(plot_dir,x,"filtered_annotation","txt", sep="."), sep="\t", quote=FALSE, col.names=TRUE, row.names=FALSE))
}
filterMousePeaks(fileList = "consensus/promoters/annotation", plot_dir = "consensus/promoters/clustered/")

#########################################################################################################################
################################################ FILTERED PEAK FEATURES #################################################
#########################################################################################################################

## Assess peak features after k-means clustering

peakFeatures <- function(fileList, peak.length.plot, peak.intensity.plot){
    files =list.files(fileList, pattern= ".txt", full.names=T) # create list of files in directory
    files = as.list(files)
    data = lapply(files, function(x) fread(x, header=TRUE, sep="\t", quote="", na.strings=c("", "NA")))
    names(data) = c("dunnart", "E10","E11","E12","E13","E14", "E15")
    df = Map(mutate, data, group = names(data))
    df = lapply(df, function(x) x=setnames(x, old="geneId", new="mouseensembl", skip_absent=TRUE) %>% as.data.table())
    df = lapply(df, function(x) x %>% dplyr::select("width", "V7", "annotation", "mouseensembl", "distanceToTSS", "group") %>% as.data.table())

    peaks = rbindlist(df,)
    peaks$cre = "promoters"

    enhancers = list.files(enhancerList, pattern= ".txt", full.names=T)
    enhancers = as.list(enhancers)
    enhancers = lapply(enhancers, function(x) fread(x, header=TRUE, sep="\t", quote = "", na.strings=c("", "NA"))) # read in all files
    names(enhancers) = c("dunnart", "E10","E11","E12","E13","E14", "E15")
    enhancers = Map(mutate, enhancers, group = names(enhancers))
    enhancers = lapply(enhancers, function(x) x=setnames(x, old="geneId", new="mouseensembl", skip_absent=TRUE) %>% as.data.table())
    enhancers = lapply(enhancers, function(x) x %>% dplyr::select("width", "V7", "annotation", "mouseensembl", "distanceToTSS", "group") %>% as.data.table())

    enhancers = rbindlist(enhancers,)
    enhancers$cre = "enhancers"

    all = rbind(peaks, enhancers)

    ## Plot peak intensity
    p <- ggplot(all, aes(factor(group), y = log10(V7))) + 
    geom_violin(aes(fill=factor(group), color=factor(group)), position = "dodge") + 
    geom_boxplot(aes(color=factor(group)),
    width = .15, outlier.shape = NA,
    #fill=c("#FCF8EC", "#FCF8EC", "#FCF8EC", "#FCF8EC", "#FCF8EC", "#FCF8EC", "#FCF8EC"),
    fill=c("#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2"),
    #fill=c("#E4F3F1", "#E4F3F1", "#E4F3F1", "#E4F3F1", "#E4F3F1", "#E4F3F1", "#E4F3F1"),
    position = position_dodge(width=.1), 
    notch=TRUE) + facet_wrap(. ~ cre, strip.position = "bottom") +
    theme(panel.spacing = unit(0, "lines"),
        strip.background = element_blank(),
        strip.placement = "outside", axis.text.x = element_text(angle = 45, , hjust = 1, size = 25) 
        ) + theme_bw() + xlab("") + ylab("Log 10 Peak Intensity") 
    
    pdf("test.pdf", width=10, height = 6)
    print(p + scale_fill_manual(values = c("#FFDD8C","#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078"))
     + scale_color_manual(values = c("#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166"))) 
    #scale_color_manual(values = c("#E9C46A","#E9C46A", "#E9C46A", "#E9C46A", "#E9C46A", "#E9C46A", "#E9C46A")) +
    #scale_color_manual(values = c("#4D004B", "#4D004B", "#4D004B", "#4D004B", "#4D004B", "#4D004B", "#4D004B")) +
    #scale_color_manual(values = c("#2A9D8F", "#2A9D8F", "#2A9D8F", "#2A9D8F", "#2A9D8F", "#2A9D8F", "#2A9D8F")) +
    #scale_fill_manual(values = c("#F1DAA2","#F1DAA2", "#F1DAA2", "#F1DAA2", "#F1DAA2", "#F1DAA2", "#F1DAA2")))
    #scale_fill_manual(values = c("#7A4078", "#7A4078", "#7A4078", "#7A4078", "#7A4078", "#7A4078", "#7A4078")))
    #scale_fill_manual(values = c("#7AC2B9", "#7AC2B9", "#7AC2B9", "#7AC2B9", "#7AC2B9", "#7AC2B9", "#7AC2B9")))
    dev.off()

    ## Plot peak lengths
    p <- ggplot(all, aes(factor(group), y = log10(width))) + 
    geom_violin(aes(fill=factor(group), color=factor(group)), position = "dodge") + 
    geom_boxplot(aes(color=factor(group)),
    width = .15, outlier.shape = NA,
    #fill=c("#FCF8EC", "#FCF8EC", "#FCF8EC", "#FCF8EC", "#FCF8EC", "#FCF8EC", "#FCF8EC"),
    fill=c("#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2"),
    #fill=c("#E4F3F1", "#E4F3F1", "#E4F3F1", "#E4F3F1", "#E4F3F1", "#E4F3F1", "#E4F3F1"),
    position = position_dodge(width=.1), 
    notch=TRUE) + facet_wrap(. ~ cre, strip.position = "bottom") +
  theme(panel.spacing = unit(0, "lines"),
        strip.background = element_blank(),
        strip.placement = "outside", axis.text.x = element_text(angle = 45, , hjust = 1, size = 25) 
        ) + theme_bw() + xlab("") + ylab("Log 10 Peak Length") 
    
    pdf("test.pdf", width=10, height = 6)
    print(p +  scale_fill_manual(values = c("#FFDD8C","#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078"))
     + scale_color_manual(values = c("#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166"))) 
    #scale_color_manual(values = c("#E9C46A","#E9C46A", "#E9C46A", "#E9C46A", "#E9C46A", "#E9C46A", "#E9C46A")) +
    #scale_color_manual(values = c("#4D004B", "#4D004B", "#4D004B", "#4D004B", "#4D004B", "#4D004B", "#4D004B")) +
    #scale_color_manual(values = c("#2A9D8F", "#2A9D8F", "#2A9D8F", "#2A9D8F", "#2A9D8F", "#2A9D8F", "#2A9D8F")) +
    #scale_fill_manual(values = c("#F1DAA2","#F1DAA2", "#F1DAA2", "#F1DAA2", "#F1DAA2", "#F1DAA2", "#F1DAA2")))
    #scale_fill_manual(values = c("#7A4078", "#7A4078", "#7A4078", "#7A4078", "#7A4078", "#7A4078", "#7A4078")))
    #scale_fill_manual(values = c("#7AC2B9", "#7AC2B9", "#7AC2B9", "#7AC2B9", "#7AC2B9", "#7AC2B9", "#7AC2B9"))
    dev.off()

    ## distance to TSS
    peaks = peaks[,log10_abs_dist:=log10(abs(distanceToTSS)+1)][,log10_abs_dist:=ifelse(distanceToTSS<0,-log10_abs_dist,log10_abs_dist)]
    pdf(file = "clusteredPromoter_TSS.pdf", width=10, height = 7)
    print(ggplot(peaks, aes(x=distanceToTSS, color = factor(group), fill = factor(group))) + 
    geom_density(alpha=0.5) + theme_bw()+ scale_y_continuous(limits=c(0,0.15), labels = scales::percent))
    dev.off()
}

peakFeatures(fileList = "consensus/promoters/clustered", enhancerList = "consensus/enhancers/annotation", peak.length.plot = "dunnart_cluster_length.pdf", 
peak.intensity.plot = "dunnart_cluster_intensity.pdf")

peakFeatures(fileList = "consensus/enhancers/annotation", peak.length.plot = "dunnart_enhancers_length.pdf", 
peak.intensity.plot = "dunnart_enhancer_intensity.pdf")

## Now look at CpG% and GC% for these groups

GCcontent = function(fileList, clusterList, enhancerList, gcPlot, cpgPlot){
  files =list.files(fileList, pattern= "_homerAnnot.txt", full.names=T) # create list of files in directory
  files = as.list(files)
  data = lapply(files, function(x) fread(x, header=TRUE, sep="\t", quote = "", na.strings=c("", "NA"))) # read in all files
  names(data) = c("dunnart", "E10","E11","E12","E13","E14", "E15")
  df1 = Map(mutate, data, group = names(data))
  clusterFiles = list.files(clusterList, pattern= "_annotation.txt", full.names=T)
  clusterData = lapply(clusterFiles, function(x) fread(x, header=TRUE, sep="\t", quote = "", na.strings=c("", "NA"))) # read in all files
  clusterIDs = lapply(clusterData, function(x) x$V4 %>% as.data.frame())
  clusterIDs = rbindlist(clusterIDs,)
  df1 = rbindlist(df1,)
  test = as.vector(unlist(clusterIDs))
  promoters = df1[df1$PeakID %in% test,]
  colnames(promoters)[20] <- "CpG"
  colnames(promoters)[21] <- "GC"
  promoters$cre = "promoters"

  enhancers = list.files(enhancerList, pattern= "_homerAnnot.txt", full.names=T)
  enhancers = as.list(enhancers)
  enhancers = lapply(enhancers, function(x) fread(x, header=TRUE, sep="\t", quote = "", na.strings=c("", "NA"))) # read in all files
  names(enhancers) = c("dunnart", "E10","E11","E12","E13","E14", "E15")
  enhancers = Map(mutate, enhancers, group = names(enhancers))

  enhancers = rbindlist(enhancers,)
  colnames(enhancers)[20] <- "CpG"
  colnames(enhancers)[21] <- "GC"
  enhancers$cre = "enhancers"
 
  all = rbind(promoters, enhancers)

  p <- ggplot(all, aes(factor(group), y = GC)) + 
    geom_violin(aes(fill=factor(group), color=factor(group)),
    position = "dodge"
    )+
  geom_boxplot(aes(color=factor(group)),
    outlier.shape = NA,
    width = .15, 
    notch = TRUE,
    #fill=c("#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#FFEEC6","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2","#D3BFD2"),
    position = position_dodge(width=.1)
  ) + facet_wrap(. ~ cre, strip.position = "bottom") +
  theme(panel.spacing = unit(0, "lines"),
        strip.background = element_blank(),
        strip.placement = "outside", axis.text.x = element_text(angle = 45, , hjust = 1, size = 25) 
        ) +
  theme_bw() + xlab("") + ylab("GC content") 
  pdf(gcPlot, width=10, height = 6)
  print(p + stat_compare_means() + scale_fill_manual(values = c("#FFDD8C","#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078")) + scale_color_manual(values = c("#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166"))) 
  dev.off()

  p <- ggplot(all, aes(factor(group), y = CpG)) + 
  geom_violin(aes(fill=factor(group), color=factor(group)),
   position = "dodge"
   )+
  geom_boxplot(aes(color=factor(group)),
    outlier.shape = NA,
    width = .15, 
    notch = TRUE,
    #fill=c("#D3BFD2","#D4C7E2","#E7EEF6", "#FCF8EC","#E4F3F1"),
    position = position_dodge(width=.1)
  ) + facet_wrap(. ~ cre, strip.position = "bottom") +
  theme(panel.spacing = unit(0, "lines"),
        strip.background = element_blank(),
        strip.placement = "outside", axis.text.x = element_text(angle = 45, , hjust = 1, size = 25) 
        ) + theme_bw() + xlab("") + ylab("CpG") 
  pdf(cpgPlot, width=10, height = 6)
  print(p + scale_fill_manual(values = c("#FFDD8C","#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C", "#FFDD8C","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078","#7A4078")) + scale_color_manual(values = c("#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#4D004B","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166","#FFD166"))) 
  dev.off()
}

GCcontent(fileList = "consensus/promoters/homerAnnot", clusterList = "consensus/promoters/clustered", enhancerList = "consensus/enhancers/homerAnnot", gcPlot ="dunnart_GC.pdf", cpgPlot = "dunnart_cpg.pdf")

## Compare peak signals between mouse and dunnart
fileList = "consensus/all"
files =list.files(fileList, pattern= ".narrowPeak", full.names=T) # create list of files in directory
files = as.list(files)
data = lapply(files, function(x) fread(x, header=FALSE, sep="\t", quote = "", na.strings=c("", "NA"))) # read in all files
names(data) = c("H3K27ac_E10.5","H3K27ac_E11.5","H3K27ac_E12.5","H3K27ac_E13.5","H3K27ac_E14.5", "H3K27ac_E15.5",
                "H3K27ac_dunnart", "H3K4me3_E10.5", "H3K4me3_E11.5", "H3K4me3_E12.5", "H3K4me3_E13.5", "H3K4me3_E14.5",
                "H3K4me3_E15.5", "H3K4me3_dunnart")
df1 = Map(mutate, data, group = names(data))
df2 = rbindlist(df1,)

my_comparisons <- list( c("H3K27ac_dunnart", "H3K27ac_E10.5"), c("H3K27ac_dunnart", "H3K27ac_E11.5"), c("H3K27ac_dunnart", "H3K27ac_E12.5"),
                          c("H3K27ac_dunnart", "H3K27ac_E13.5"), c("H3K27ac_dunnart", "H3K27ac_E14.5"), c("H3K27ac_dunnart", "H3K27ac_E15.5"),
                          c("H3K4me3_dunnart", "H3K4me3_E10.5"), c("H3K4me3_dunnart", "H3K4me3_E11.5"), c("H3K4me3_dunnart", "H3K4me3_E12.5"),
                          c("H3K4me3_dunnart", "H3K4me3_E13.5"), c("H3K4me3_dunnart", "H3K4me3_E14.5"), c("H3K4me3_dunnart", "H3K4me3_E15.5"))

p <- ggplot(df2, aes(factor(group), y = log10(V7))) + 
    geom_violin(aes(fill=factor(group))
    )+
    geom_boxplot(aes(color=factor(group)),
    width = .15, 
    outlier.shape = NA,
    position = position_dodge(width=.1)
    ) + theme_bw() + xlab("") + ylab("Log10 Peak Intensity") 
    pdf(file=paste0(plot_dir, peak.intensity.plot), width = 10, height = 7)
    print(p + stat_compare_means(comparisons = my_comparisons))
    dev.off()  